/*
 * Description: Business, Corporate, Portfolio and Blog Theme.
 * Version: 1.4
 * Author: @htmlstream
 * Website: http://htmlstream.com
*/
var Index = function () {

    return {
        
        //Parallax Slider
        initParallaxSlider: function () {
            $(function() {
                $('#da-slider').cslider({
                    autoplay	: true,
                    bgincrement	: 450
                });
            });
        }
    };
}();

var App = function () {
    function handleIEFixes() {
        //fix html5 placeholder attribute for ie7 & ie8
        if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) < 9) { // ie7&ie8
            jQuery('input[placeholder], textarea[placeholder]').each(function () {
                var input = jQuery(this);

                jQuery(input).val(input.attr('placeholder'));

                jQuery(input).focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                jQuery(input).blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }
    
    return {
        init: function () {
            handleIEFixes();
        },
        
        initParallaxBg: function () {
            jQuery('.parallaxBg').parallax("50%", 0.2);
            jQuery('.parallaxBg1').parallax("50%", 0.4);
        }
    };
}();

$(document).ready(function(){  
             
    $('#country_code').change(function(){
        var code = $(this).val();
        $('.dial_code').html(code);
        $('#dial_code').val(code);
    });
                
    $('#signupform').validate({
        rules: {
            "name": {
                required: true,
                nameRegex: true
            }
        }
    });
    $("#name").show().focus();
    
    // Div Left Column Tab Script
    $('.div_tabs li').click(function(){
        $('.dvDataContainer').hide();        
        $('.div_tabs li').removeClass('selected');        
        $(this).addClass('selected');        
        var div_id = $(this).children('a').attr('class');       
        $('#'+div_id).show();
    });
    $('.fn_accordian').click(function(){
        $('.fn_accordian_li').removeClass('selected');
        $(this).parent('li').addClass('selected');
        $('.fn_acr_content').hide();
        $(this).siblings('.fn_acr_content').show();        
    });
    
    $('#fn_language').change(function(){       
        var val = $(this).val(); 
         if(val)
         {
              $('.fn_video_block').hide();
              $('.'+val).show();
         } 
    });
    
    $(".fn_close").click(function () {
        $("#divWelcomeThis").hide();
    });
        
                 
    $.validator.addMethod("nameRegex", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z. ]+$/);
    },"Please enter only alpha character.");
        
    
    
    $("div.leadtext p+span").click(function() {
        $('html, body').animate({
            scrollTop: parseInt($("div.content-wrap").offset().top - 100)
        }, 2500);
    });
                
    $("#downloadSlider").carousel();
                
    $("header div.hamburger").click(function(){
        $(this).next('ul').toggle({
            'slow':'3000'
        });
    });
                
    $(".button-dismiss").click(function(){
        $("div.download-this").hide();
    });  
    
   $('#contactform').validate(); 
});

   function limitText(limitField, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            $('#countdown').html(limitNum - limitField.value.length);
        }
    }