<?php

include 'class.upload.php';

class Photo_upload
{
    function process($upload_array)
    {
        $large_dir = "assets/images/photos/";

        $handle = new upload($upload_array['files_array']);

        if($handle->uploaded)
        {
            $handle->file_name_body_pre = $upload_array['image_prefix'];
            $handle->image_resize = true;
            $handle->image_ratio_crop = true;
            $handle->image_x = 120;
            $handle->image_y = 140;
            $handle->allowed = array('image/*');

            $handle->Process($large_dir);


            if($handle->processed)
            {
                $image_name = $handle->file_dst_name;
                $handle->clean();
                return $image_name;
            }
            else
            {
                error($handle->error);
                redirect($upload_array['redirect_url']);
                exit;
            }
        }
    }
}