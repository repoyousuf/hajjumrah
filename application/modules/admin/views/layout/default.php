<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title_for_layout; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->    
        <link href="<?php echo BOOTSTRAP_URL; ?>css/bootstrap.css" rel="stylesheet"/>    
        <link href="<?php echo BOOTSTRAP_URL; ?>css/bootstrap-responsive.css" rel="stylesheet"/> 

        <link href="<?php echo CSS_URL; ?>admin.css" media="screen" rel="stylesheet" type="text/css"/>
        <script src="<?php echo JS_URL; ?>jquery-1.7.1.js"></script>
        <!--<script src="js/bootstrap.min.js"></script>-->

    </head>
    <body>       

        <?php $this->load->view('layout/header'); ?> 
        
        <div  class="container" id="container"> 
            <div class="row-fluid" style="height: 5px;"> </div> 
            <div class="row-fluid">
                <?php echo $content_for_layout; ?>
            </div>
        </div>

        <?php $this->load->view('layout/footer'); ?>  
    </body>
</html>