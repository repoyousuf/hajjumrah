<div id="menu">
    <ul class="menu">
        <li ><a href="<?php echo base_url('admin/dashboard'); ?>" ><span>Home</span></a></li> &nbsp; |
        <li><a href="<?php echo site_url('admin/employee/profile'); ?>"><span>Profile</span></a></li> &nbsp; |
        <?php if($this->session->userdata('user_type') == 2){ ?>         
        <li ><a href="<?php echo site_url('admin/project/preport'); ?>" ><span>Performance</span></a></li> &nbsp; |
        <li ><a href="<?php echo site_url('admin/roster/tsreport'); ?>" ><span>Timesheet</span></a></li> &nbsp; |
        <li class="arrow-bottom"><a href="javascript:void(0);" title=""><span>Task</span></a>
            <div>
                <ul>
                    <li><a href="<?php echo site_url('admin/assigntask/tasks/todays-task'); ?>"><span>Today Task </span></a>                     
                    </li>            
                    <li><a href="<?php echo site_url('admin/assigntask/tasks/overdue-task'); ?>"><span>Overdue Task</span></a>                        
                    </li>            
                    <li><a href="<?php echo site_url('admin/assigntask/tasks/completed-task'); ?>"><span>Completed Task</span></a>                      
                    </li>            
                    <li><a href="<?php echo site_url('admin/assigntask/tasks/future-task'); ?>"><span>Future Task</span></a>                      
                    </li>            
                </ul>
            </div>                    
        </li> &nbsp; |
       
        <?php } ?>

         <?php if($this->session->userdata('user_type') == 1){ ?>
        <li class="arrow-bottom"><a href="#" title=""><span>Employee</span></a>
            <div>
                <ul>
                    <li><a href="<?php echo site_url('admin/employee'); ?>"><span>Employee</span></a></li>            
                    <li><a href="<?php echo site_url('admin/employee/add'); ?>"><span>Add New Employee</span></a></li>            
                    <li><a href="<?php echo site_url('admin/employee/tasks'); ?>"><span>Task Report</span></a></li>            
                    <li><a href="<?php echo site_url('admin/roster/index'); ?>"><span>Work Roster</span></a></li>            
                    <li><a href="<?php echo site_url('admin/roster/add'); ?>"><span>Create Work Roster</span></a></li>            
                    <li><a href="<?php echo site_url('admin/roster/tsreport'); ?>"><span>Timesheet report</span></a></li>            
                </ul>
            </div>   
        </li> &nbsp; |
        <li class="arrow-bottom"><a href="#" title="Computer Bazar"><span>Projects</span></a>
            <div>
                <ul>
                    <li><a href="<?php echo site_url('admin/project'); ?>"><span>Projects</span></a></li>            
                    <li><a href="<?php echo site_url('admin/project/add'); ?>"><span>Add New Project</span></a></li>            
                    <li><a href="<?php echo site_url('admin/module/'); ?>"><span>Modules</span></a></li>            
                    <li><a href="<?php echo site_url('admin/module/add'); ?>"><span>Add New Module</span></a></li>            
                    <li><a href="<?php echo site_url('admin/assigntask/add'); ?>"><span>Assign Task</span></a></li>            
                    <li><a href="<?php echo site_url('admin/project/preport'); ?>"><span>Performance Report</span></a></li>            
                </ul>
            </div>   
        </li> &nbsp; |
        <li ><a href="<?php echo site_url('admin/project/cinvoice'); ?>" ><span>Client Invoice</span></a></li> &nbsp; |
        <?php } ?>
         <li class="arrow-bottom"><a href="javascript:void(0);" title=""><span>Message</span></a>
                             
        </li>  &nbsp; |       
        <li><a href="<?php echo site_url('admin/logout'); ?>"><span>Logout (  Logged in as <?php echo $this->session->userdata('full_name' );  ?>)</span></a></li>
    </ul>
</div>