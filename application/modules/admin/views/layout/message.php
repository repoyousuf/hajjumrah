<style type="text/css">
    .alert {
        padding: 8px 35px 8px 14px;
        margin-bottom: 20px;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
        background-color: #fcf8e3;
        border: 1px solid #fbeed5;
        /*-webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;*/
    }
    .alert,
    .alert h4 {
        color: #c09853;
    }
    .alert h4 {
        margin: 0;
    }
    .alert .close {
        position: relative;
        top: -2px;
        right: -21px;
        line-height: 20px;
    }
    .alert-success {
        color: #468847;
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }
    .alert-success h4 {
        color: #468847;
    }
    .alert-danger,
    .alert-error {
        color: #b94a48;
        background-color: #f2dede;
        border-color: #eed3d7;
    }
    .alert-danger h4,
    .alert-error h4 {
        color: #b94a48;
    }
    .alert-info {
        color: #3a87ad;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }
    .alert-info h4 {
        color: #3a87ad;
    }
    .alert-block {
        padding-top: 14px;
        padding-bottom: 14px;
    }
    .alert-block > p,
    .alert-block > ul {
        margin-bottom: 0;
    }
    .alert-block p + p {
        margin-top: 5px;
    }
    .remove{font-weight: bold; font-size: 12px;cursor: pointer; }
</style>

<?php
$message = '';
$alert_class = 'success';
if ($this->session->userdata('success')):
    $message = $this->session->userdata('success');
    $this->session->unset_userdata('success');
    $class = 'success';
elseif ($this->session->userdata('error')):
    $message = $this->session->userdata('error');
    $this->session->unset_userdata('error');
    $class = 'error';
elseif ($this->session->userdata('warning')):
    $message = $this->session->userdata('warning');
    $this->session->unset_userdata('warning');
    $class = 'warning';
elseif ($this->session->userdata('info')):
    $message = $this->session->userdata('info');
    $this->session->unset_userdata('alert_info');
    $class = 'info';
endif;
?>

<?php
if ($message):
    ?>
    <div id="message_div" class="alert alert-<?php echo $class; ?>" style="padding: 10px 30px 10px 7px; margin-bottom: 0;margin-top: 25px;">
        <ul style="margin: 0; list-style-type: none;width: 99%;float: left;">
            <?php
            echo $message;
            ?>          
        </ul>
        <span class="remove">X</span>
    </div>
    <?php
endif;
?>

<script>
    $(function(){
        $('#message_div').delay(10000).fadeOut();
        $('.remove').click(function(){           
            $('#message_div').hide();
        });
    });    
</script>