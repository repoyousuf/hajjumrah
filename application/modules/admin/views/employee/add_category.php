<div class="span12">
    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span6">Add News Category</div>
            <div class="span6"><a class="add-new" href="<?php echo site_url('admin/news/categories'); ?>">News Categories</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <?php
                $form_action = 'admin/news/add_category';
                $hidden = array();
                if(isset($category) && !empty ($category))
                {
                    $form_action = 'admin/news/edit_category';
                    $hidden['id'] = $category->id;
                }
            ?>
            <?php echo form_open_multipart($form_action, '', $hidden); ?>
            <table width="100%" border="0" class="table-form feedback" >
                <tr>
                    <th></th>
                    <td>Fields with <span class="required">*</span> are required.</td>
                    <td></td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <th><label for="news_category">Category Name : <span class="required">*</span></label></th>
                    <td><input type="text" name="news_category" id="news_category" class="required" value="<?php echo set_value('news_category', isset($category) && isset($category->news_category) ? $category->news_category : ''); ?>"/></td>
                    <td><?php echo form_error('news_category'); ?></td>
                </tr>
                <tr>                
                <?php echo form_error('is_featured'); ?>
                    <th><label for="is_featured">Is Featured :</label></th>
                    <td><input  type="checkbox" name="is_featured" id="is_featured" value="1" <?php echo (isset($category->is_featured) && $category->is_featured == 1 ) ? 'checked="checked"' : ''; ?> /></td>
                    <td><?php echo form_error('is_featured'); ?></td>
                </tr>
                 <tr>
                    <th><label for="meta_keyword">Meta Keywords : <span class="required">*</span></label></th>
                    <td><textarea  name="meta_keyword" id="meta_keyword" class="required" style="width: 457px; height: 60px;" ><?php echo set_value('meta_keyword', isset($category) && isset($category->meta_keyword) ? $category->meta_keyword : ''); ?></textarea></td>
                    <td><?php echo form_error('meta_keyword'); ?></td>
                </tr>
                <tr>
                    <th><label for="meta_description">Meta Description : <span class="required">*</span></label></th>
                    <td><textarea  name="meta_description" id="meta_description" class="required" style="width: 457px; height: 60px;" ><?php echo set_value('meta_description', isset($category) && isset($category->meta_description) ? $category->meta_description : ''); ?></textarea></td>
                    <td><?php echo form_error('meta_description'); ?></td>
                </tr>
                <?php if(isset($category) && !empty ($category)): ?>
                 <tr>
                    <th><label for="status">Status:<span class="required">*</span></label></th>
                    <td>
                        <select id="status" name="status" style="width: 120px;">
                            <option value="">--Select--</option>
                            <option value="1" <?php echo set_select('status', '1', $category->status == 1 ? TRUE : FALSE); ?>>Active</option>                                        
                            <option value="0" <?php echo set_select('status', '0', $category->status == 0 ? TRUE : FALSE); ?>>Inactive</option>                                        
                        </select>
                    </td>
                    <td><?php echo form_error('status'); ?></td>
                </tr>
                 <?php endif; ?>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                       <input  type="submit" class="btn btn-primary" id="submit"  name="submit" value="Save"/>
                       <input type="reset" class="btn btn-primary" id="reset"  name="reset" value="Reset"/>
                       <input type="button" class="btn btn-primary"  id="cancel" name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url('admin/mobile/categories'); ?>'" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <?php echo form_close(); ?>   
        </div>                
    </div>   
</div>

