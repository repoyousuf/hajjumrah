<div class="span12">

    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span6">Employee Details</div>
            <div class="span6"><a class="add-new" href="<?php echo site_url('admin/employee/index'); ?>">Employees</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <fieldset>
            <legend>Profile Info</legend>
            <div class="span5 body-content"> 
                <table width="100%" border="0" class="table-form feedback" >                         
                    <tr>
                        <th><label for="full_name">Full Name : </label></th>
                        <td><?php echo $employee->full_name; ?></td>                    
                    </tr>

                    <tr>
                        <th><label for="present_address">Present Address : </label></th>
                        <td><?php echo $employee->present_address; ?></td>
                    </tr>

                    <tr>
                        <th><label for="permanent_address">Permanent Address : </label></th>
                        <td><?php echo $employee->permanent_address; ?></td>
                    </tr>               

                    <tr>
                        <th><label for="contact_number">Contact Number : </label></th>
                        <td><?php echo $employee->contact_number; ?></td>
                    </tr>

                    <tr>
                        <th><label for="email_addresses">Email Addresses :</label></th>
                        <td><?php echo $employee->email_addresses; ?></td>
                    </tr>              

                    <tr>
                        <th><label for="url">Url : </label></th>
                        <td><?php echo $employee->url ?></td>
                    </tr> 
                    <tr>
                        <th><label for="facebook">Facebook ID : </label></th>
                        <td><?php echo $employee->facebook; ?></td>
                    </tr>                
                    <tr>
                        <th><label for="messenger">Messenger :</label></th>
                        <td><?php echo $employee->messenger; ?></td>
                    </tr>

                    <tr>
                        <th><label for="education">Education : </label></th>
                        <td><?php echo $employee->education; ?></td>
                    </tr>

                    <tr>
                        <th><label for="experience">Experience :</label></th>
                        <td><?php echo $employee->experience; ?></td>
                    </tr>               

                    
                </table>
            </div>
            <div class="span6 body-content"> 
                <table width="100%" border="0" class="table-form feedback" > 

                    <tr>
                        <th style="width: 50%"><label for="salary">Salary :</label></th>
                        <td style="width: 50%"><?php echo $employee->salary; ?></td>
                    </tr> 

                    <tr>
                        <th><label for="employment_date">Employment Date :</label></th>
                        <td><?php echo date('F j, Y', strtotime($employee->employment_date)); ?></td>
                    </tr> 

                    <tr>
                        <th><label for="job_type">Job Type:</label></th>
                        <td><?php echo $employee->job_type; ?></td>
                    </tr>                               
                    <tr>
                        <th><label for="password">Photo :</label></th>
                        <td> 
                            <?php if ($employee->photo) { ?>
                                <img src="<?php echo base_url() . 'assets/images/photos/' . $employee->photo; ?>" />
                            <?php } ?>
                        </td>
                    </tr>                 
                    <tr>
                        <th><label for="password">Resume :</label></th>
                        <td>
                            <?php if ($employee->resume) { ?>
                                <a href="<?php echo base_url() . 'assets/images/resume/' . $employee->resume; ?>"><?php echo $employee->resume; ?></a>
                            <?php } ?>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="email">Login Email :</label></th>
                        <td><?php echo $employee->email; ?></td>
                    </tr>              
                    <tr>
                        <th><label for="status">Status:</label></th>
                        <td>
                            <?php echo $employee->status == 1 ? 'Active' : 'Inactive'; ?>
                        </td>
                    </tr>
                </table>
            </div>        
        </fieldset>

    </div>                
</div>   
</div>