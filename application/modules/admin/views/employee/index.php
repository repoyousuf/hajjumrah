<div class="span12">
     <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span3">Employees</div>
            <div class="span6 filter-form">
          
            </div>
            <div class="span3"><a class="add-new" href="<?php echo site_url('admin/employee/add'); ?>">Add New</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <table width="100%" border="1" class="table-grid" >
                <tr>
                    <th>#SL</th>
                    <th>User Type</th>
                    <th>Name</th>
                    <th>Present Address</th>
                    <th>Education</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Employment Date</th>
                    <th>Job Type</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                <?php $count = $count + 1; if(isset ($employees) && !empty ($employees)): ?>
                <?php foreach($employees as $obj ) : ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $obj->user_type == 1 ? 'Admin' : 'Employee'; ?></td>
                    <td><?php echo $obj->full_name; ?></td>
                    <td><?php echo $obj->present_address; ?></td>
                    <td><?php echo $obj->education; ?></td>
                    <td><?php echo $obj->experience; ?></td>
                    <td><?php echo $obj->salary; ?></td>
                    <td><?php echo date('F j, Y', strtotime($obj->employment_date)); ?></td>
                    <td><?php echo $obj->job_type; ?></td>
                    <td><?php echo $obj->status == 1 ? 'Active' : 'Inactive'; ?></td>
                    <td>
                        <a href="<?php echo site_url('admin/employee/details/'.$obj->id); ?>">Details</a> |
                        <a href="<?php echo site_url('admin/employee/edit/'.$obj->id); ?>">Edit</a> |
                        <a href="<?php echo site_url('admin/employee/delete/'.$obj->id); ?>" onclick="javascript: return confirm('Are you sure you want to delete this news?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="11"> No employees record found.</td>
                </tr>
                <?php endif; ?>
            </table>
             <div class="paging paging-right"><?php echo $pagination; ?></div>
        </div>                
    </div>   
</div>

