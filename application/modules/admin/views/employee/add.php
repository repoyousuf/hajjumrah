<div class="span12">
      <style type="text/css">
        .table-form tr th {width: 25%;}
    </style>
<link type="text/css" href="<?php echo base_url(); ?>datepicker/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>datepicker/ui/ui.datepicker.js"></script>

 <script type="text/javascript">
        $(function() {
         $("#employment_date").datepicker({changeMonth: true,changeYear: true,showOn: 'both', buttonImage: '<?php echo base_url(); ?>assets/images/calendar.gif', buttonImageOnly: true});
      }); 
    
  </script>    
    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span6">Add Employee </div>
            <div class="span6"><a class="add-new" href="<?php echo site_url('admin/employee/index'); ?>">Employees</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <?php
                $form_action = 'admin/employee/add';
                $hidden = array();
                if(isset($employee) && !empty ($employee))
                {
                    $form_action = 'admin/employee/edit';
                    $hidden['id'] = $employee->id;
                }
            ?>
            <?php echo form_open_multipart($form_action, '', $hidden); ?>
            <table width="100%" border="0" class="table-form feedback" >
                <tr>
                    <th width="25%"></th>
                    <td width="50%">Fields with <span class="required">*</span> are required.</td>
                    <td width="25%"></td>
                </tr>
                <tr><td>&nbsp;</td><td colspan="2" ><div class="fn-duplicate-image"></div></td></tr>                
               <tr>
                    <th><label for="user_type">User Type : <span class="required">*</span></label></th>
                    <td>
                        <select id="user_type" name="user_type" class="required" style="width: auto; height: auto;">
                            <option value="">--Select--</option>
                            <option value="1" <?php echo isset($employee) && ($employee->user_type == 1) ?  'selected="selected"' : ''; ?> >Admin</option>                                        
                            <option value="2" <?php echo isset($employee) && ($employee->user_type == 2) ?  'selected="selected"' : ''; ?>>Employee</option> 
                     </select>
                    </td>
                    <td><?php echo form_error('user_type'); ?></td>
                </tr>
                
               <tr>
                    <th><label for="full_name">Full Name : <span class="required">*</span></label></th>
                    <td><input type="text" name="full_name" id="full_name" class="required"  value="<?php echo set_value('full_name', isset($employee) && isset($employee->full_name) ? $employee->full_name : ''); ?>" /></td>
                    <td><?php echo form_error('full_name'); ?></td>
                </tr>
                
               <tr>
                    <th><label for="present_address">Present Address : <span class="required">*</span></label></th>
                    <td><input type="text" name="present_address" id="present_address" class="required"  value="<?php echo set_value('present_address', isset($employee) && isset($employee->present_address) ? $employee->present_address : ''); ?>" /></td>
                    <td><?php echo form_error('present_address'); ?></td>
                </tr>
                
               <tr>
                    <th><label for="permanent_address">Permanent Address : <span class="required">*</span></label></th>
                    <td><input type="text" name="permanent_address" id="permanent_address" class="required"  value="<?php echo set_value('permanent_address', isset($employee) && isset($employee->permanent_address) ? $employee->permanent_address : ''); ?>" /></td>
                    <td><?php echo form_error('permanent_address'); ?></td>
                </tr>
                
              
                <tr>
                    <th><label for="contact_number">Contact Number : <span class="required">*</span></label></th>
                    <td><textarea  name="contact_number" id="contact_number" class="required" style="height: 100px;"><?php echo set_value('contact_number', isset($employee) && isset($employee->contact_number) ? $employee->contact_number : ''); ?></textarea></td>
                    <td><?php echo form_error('contact_number'); ?></td>
                </tr>
                
                <tr>
                    <th><label for="email_addresses">Email Addresses : <span class="required">*</span></label></th>
                    <td><textarea  name="email_addresses" id="email_addresses" class="required" style="height: 100px;"><?php echo set_value('email_addresses', isset($employee) && isset($employee->email_addresses) ? $employee->email_addresses : ''); ?></textarea></td>
                    <td><?php echo form_error('email_addresses'); ?></td>
                </tr>
                
               
                <tr>
                    <th><label for="url">Url : <span class="required">*</span></label></th>
                    <td><input type="text" name="url" id="url" class="required"  value="<?php echo set_value('url', isset($employee) && isset($employee->url) ? $employee->url : ''); ?>" /></td>
                    <td><?php echo form_error('url'); ?></td>
                </tr> 
                
                <tr>
                    <th><label for="facebook">Facebook ID : <span class="required">*</span></label></th>
                    <td><input type="text" name="facebook" id="facebook" class="required"  value="<?php echo set_value('facebook', isset($employee) && isset($employee->facebook) ? $employee->facebook : ''); ?>" /></td>
                    <td><?php echo form_error('facebook'); ?></td>
                </tr>
                
                 <tr>
                    <th><label for="messenger">Messenger : <span class="required">*</span></label></th>
                    <td><textarea  name="messenger" id="messenger" class="required" style="height: 100px;"><?php echo set_value('messenger', isset($employee) && isset($employee->messenger) ? $employee->messenger : ''); ?></textarea></td>
                    <td><?php echo form_error('messenger'); ?></td>
                </tr>
                
                 <tr>
                    <th><label for="education">Education : <span class="required">*</span></label></th>
                    <td><textarea  name="education" id="education" class="required" style="height: 100px;"><?php echo set_value('education', isset($employee) && isset($employee->education) ? $employee->education : ''); ?></textarea></td>
                    <td><?php echo form_error('education'); ?></td>
                </tr>
                
                 <tr>
                    <th><label for="experience">Experience : <span class="required">*</span></label></th>
                    <td><textarea  name="experience" id="experience" class="required" style="height: 100px;"><?php echo set_value('experience', isset($employee) && isset($employee->experience) ? $employee->experience : ''); ?></textarea></td>
                    <td><?php echo form_error('experience'); ?></td>
                </tr>               
                
                <tr>
                    <th><label for="salary">Salary : <span class="required">*</span></label></th>
                    <td><input type="text" name="salary" id="salary" class="required"  value="<?php echo set_value('salary', isset($employee) && isset($employee->salary) ? $employee->salary : ''); ?>" /></td>
                    <td><?php echo form_error('salary'); ?></td>
                </tr> 
                
                <tr>
                    <th><label for="employment_date">Employment Date : <span class="required">*</span></label></th>
                    <td><input type="text" name="employment_date" id="employment_date" class="required"  value="<?php echo set_value('employment_date', isset($employee) && isset($employee->employment_date) ? $employee->employment_date : ''); ?>" /></td>
                    <td><?php echo form_error('employment_date'); ?></td>
                </tr> 
                
                <tr>
                    <th><label for="job_type">Job Type:<span class="required">*</span></label></th>
                    <td>
                        <select id="job_type" name="job_type" style="width:auto;">
                            <option value="">--Select--</option>
                            <option value="Full Time" <?php echo set_select('job_type', 'Full Time', @$employee->job_type == 1 ? TRUE : FALSE); ?>>Full Time</option>                                        
                            <option value="Part Time" <?php echo set_select('job_type', 'Part Time', @$employee->job_type == 0 ? TRUE : FALSE); ?>>Part Time</option>                                        
                        </select>
                    </td>
                    <td><?php echo form_error('job_type'); ?></td>
                </tr>
                
                               
                <tr>
                    <th><label for="password">Photo : <span class="required">*</span></label></th>
                    <td>
                        <input type="file" name="photo" id="photo" class="required"   />
                        <input type="hidden" name="prev_resume" id="prev_resume" value="<?php echo isset($employee) && isset($employee->photo) ? $employee->photo : '' ?>" />
                    </td>
                    <td><?php echo form_error('photo'); ?></td>
                </tr>                 
                <tr>
                    <th><label for="password">Resume : <span class="required">*</span></label></th>
                    <td>
                        <input type="file" name="resume" id="resume"  class="required"/>
                        <input type="hidden" name="prev_resume" id="prev_resume" value="<?php echo isset($employee) && isset($employee->resume) ? $employee->resume : '' ?>" />
                    </td>
                    <td><?php echo form_error('resume'); ?></td>
                </tr>
                
                
                <?php if(!isset($employee) && empty ($employee)): ?>
                <tr>
                    <th><label for="email">Login Email : <span class="required">*</span></label></th>
                    <td><input type="text" name="email" id="email" class="required"  value="<?php echo set_value('email', isset($employee) && isset($employee->email) ? $employee->email : ''); ?>" /></td>
                    <td><?php echo form_error('email'); ?></td>
                </tr> 
                <tr>
                    <th><label for="password">Password : <span class="required">*</span></label></th>
                    <td><input type="text" name="password" id="password" class="required"  value="<?php echo set_value('password', isset($employee) && isset($employee->password) ? $employee->password : ''); ?>" /></td>
                    <td><?php echo form_error('password'); ?></td>
                </tr>                 
               <?php endif; ?>
               
                <?php if(isset($employee) && !empty ($employee)): ?>
                 <tr>
                    <th><label for="status">Status:<span class="required">*</span></label></th>
                    <td>
                        <select id="status" name="status" style="width:auto;">
                            <option value="">--Select--</option>
                            <option value="1" <?php echo set_select('status', '1', $employee->status == 1 ? TRUE : FALSE); ?>>Active</option>                                        
                            <option value="0" <?php echo set_select('status', '0', $employee->status == 0 ? TRUE : FALSE); ?>>Inactive</option>                                        
                        </select>
                    </td>
                    <td><?php echo form_error('status'); ?></td>
                </tr>
                 <?php endif; ?>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                       <input  type="submit" class="btn btn-primary" id="submit"  name="submit" value="Save"/>
                       <input type="reset" class="btn btn-primary" id="reset"  name="reset" value="Reset"/>
                       <input type="button" class="btn btn-primary"  id="cancel" name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url('admin/project/index'); ?>'" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <?php echo form_close(); ?>   
        </div>                
    </div>   
</div>