<div class="span12">

    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span6">Employee Profile Details</div>
            <div class="span6"></div>
        </div>
    </div>
    <div class="row-fluid">     
         
            <fieldset>
             <legend>Profile Info</legend>
              <div class="span4 body-content"> 
              <table width="100%" border="0" class="table-form feedback" > 
                  
               <tr>
                    <th width="50%"><label for="full_name">User Photo: </label></th>
                    <td> <?php $path = $employee->photo ? $employee->photo : 'defaults.jpg'; ?><img width="120" height="140" src="<?php echo IMG_URL.'photos/'.$path; ?>" alt=""</td>                    
                </tr>   
               <tr>
                    <th><label for="full_name">User Type: </label></th>
                    <td><?php echo $employee->user_type == 1 ? 'Admin' : 'Employee'; ?></td>                    
                </tr>   
               <tr>
                    <th><label for="full_name">Full Name: </label></th>
                    <td><?php echo $employee->full_name; ?></td>                    
                </tr>
               <tr>
                    <th><label for="full_name">Present Address: </label></th>
                    <td><?php echo $employee->present_address; ?></td>                    
                </tr>
               <tr>
                    <th><label for="full_name">Permanent Address: </label></th>
                    <td><?php echo $employee->permanent_address; ?></td>                    
                </tr>               
               <tr>
                    <th><label for="full_name">Contact Number: </label></th>
                    <td><?php echo $employee->contact_number; ?></td>                    
                </tr> 
                <tr>
                    <th><label for="full_name">Email Addresses: </label></th>
                    <td><?php echo $employee->email_addresses; ?></td>                    
                </tr>
                
            </table>
              </div>
                   <div class="span4 body-content"> 
              <table width="100%" border="0" class="table-form feedback" > 
                 <tr>
                    <th width="50%"><label for="full_name">Url: </label></th>
                    <td><?php echo $employee->url; ?></td>                    
                </tr>
                <tr>
                    <th><label for="full_name">Facebook: </label></th>
                    <td><?php echo $employee->facebook; ?></td>                    
                </tr>
               <tr>
                    <th><label for="full_name">Messenger: </label></th>
                    <td><?php echo $employee->messenger; ?></td>                    
                </tr> 
               <tr>
                    <th><label for="full_name">Eeducation: </label></th>
                    <td><?php echo $employee->education; ?></td>                    
                </tr>
               <tr>
                    <th><label for="full_name">Experience: </label></th>
                    <td><?php echo $employee->experience; ?></td>                    
                </tr>
               
                <tr>
                    <th><label for="full_name">Salary: </label></th>
                    <td><?php echo $employee->salary; ?></td>                    
                </tr>          
                <tr>
                    <th><label for="full_name">Employment Date: </label></th>
                    <td><?php echo date('F j, Y', strtotime($employee->employment_date)); ?></td>                    
                </tr>          
                <tr>
                    <th><label for="full_name">Job Type: </label></th>
                    <td><?php echo $employee->job_type; ?></td>                    
                </tr>          
                <tr>
                    <th><label for="full_name">Login Email: </label></th>
                    <td><?php echo $employee->email; ?></td>                    
                </tr>          
                <tr>
                    <th><label for="full_name">Created: </label></th>
                    <td><?php echo date('F j, Y', strtotime($employee->created)); ?></td>                    
                </tr>          
                <tr>
                    <th><label for="full_name">Modified: </label></th>
                    <td><?php echo date('F j, Y', strtotime($employee->modified)); ?></td>                    
                </tr>          
            </table>
                  </div>        
            </fieldset>         
                     
     </div>         
  
  <div class="row-fluid">&nbsp;</div>
</div>
