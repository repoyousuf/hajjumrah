
<div class="span12">
    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span3">News Categories</div>
            <div class="span6 filter-form"> 
            </div>
            <div class="span3"><a class="add-new" href="<?php echo site_url('admin/news/add_category'); ?>">Add New</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <table width="100%" border="1" class="table-grid" >
                <tr>
                    <th>#SL</th>
                    <th>Category name</th>
                    <th>Category Slug</th>
                    <th>Is Featured</th>
                    <th>Status</th>
                    <th width="30%">Meta Keyword</th>
                    <th>Modified</th>
                    <th>Action</th>
                </tr>
                <?php $count = $count + 1; if(isset ($categories) && !empty ($categories)): ?>
                <?php foreach($categories as $obj ) : ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $obj->news_category; ?></td>
                    <td><?php echo $obj->news_category_slug; ?></td>
                    <td><?php echo $obj->is_featured == 1 ? 'Yes' : 'No'; ?></td>
                    <td><?php echo $obj->status == 1 ? 'Active' : 'Inactive'; ?></td>
                    <td><?php echo $obj->meta_keyword; ?></td>
                    <td><?php echo $obj->modified; ?></td>
                    <td>
                        <a href="<?php echo site_url('admin/news/edit_category/'.$obj->id); ?>">Edit</a> |
                        <a href="<?php echo site_url('admin/news/delete_category/'.$obj->id); ?>" onclick="javascript: return confirm('Are you sure you want to delete this news category?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="9"> No news category record found.</td>
                </tr>
                <?php endif; ?>
            </table>
            <div class="paging paging-right"><?php echo $pagination; ?></div>
        </div>                
    </div>   
</div>