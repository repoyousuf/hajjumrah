<div class="span12">
  <link type="text/css" href="<?php echo base_url(); ?>datepicker/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>datepicker/ui/ui.datepicker.js"></script>

 <script type="text/javascript">
        $(function() {
         $("#start_date").datepicker({changeMonth: true,changeYear: true,showOn: 'both', buttonImage: '<?php echo base_url(); ?>assets/images/calendar.gif', buttonImageOnly: true});
      }); 
        $(function() {
         $("#end_date").datepicker({changeMonth: true,changeYear: true,showOn: 'both', buttonImage: '<?php echo base_url(); ?>assets/images/calendar.gif', buttonImageOnly: true});
      }); 
  
    
  </script>
     <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span3">Employee Task Reports</div>
            <div class="span9 filter-form">
                <?php echo form_open_multipart(site_url('admin/employee/tasks'), '', ''); ?>
                    Employee : 
                    <select name="employee_id"  id="employee_id">
                        <option value="">--Select--</option>
                        <?php foreach($employees as $obj){ ?>
                        <option value="<?php echo $obj->id; ?>" <?php if($obj->id == $emp_id){ echo 'selected="selected"';} ?>><?php echo $obj->full_name; ?></option>
                        <?php } ?>
                    </select>
                    From : <input type="text" name="start_date" id="start_date" value="<?php echo $start_date; ?>" style="width: auto;" />
                    To : <input type="text" name="end_date" id="end_date" value="<?php echo $end_date; ?>" style="width: auto;" />
                    <input type="submit" name="submit" id="submit" value=" Show " style="width: auto;" />
                 <?php echo form_close(); ?>  
            </div>           
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <table width="100%" border="1" class="table-grid" >
                <tr>
                    <th>#SL</th>
                    <th>Project No</th>
                    <th>Project Name</th>
                    <th>Module Name</th>
                    <th>Assign On</th>
                    <th>AH</th>
                    <th>Task</th>
                    <th>Note</th>
                    <th>Deadline</th>
                    <th>Status</th>                    
                    <th>Priority</th>                    
                </tr>
                <?php $count = $count + 1; if(isset ($tasks) && !empty ($tasks)): ?>
                <?php foreach($tasks as $obj ) : ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $obj->project_no; ?></td>
                    <td><?php echo $obj->project_name; ?></td>
                    <td><?php echo $obj->module_name; ?></td>
                    <td><?php echo date('F j, Y', strtotime($obj->created)); ?></td>
                    <td><?php echo $obj->hour_per_day; ?></td>
                    <td><a href="<?php echo site_url('admin/assigntask/details/'.$this->corefunc->encode($obj->id)); ?>"><?php echo $obj->tasks; ?></a></td>
                    <td><?php echo $obj->note; ?></td>
                    <td><?php echo date('F j, Y', strtotime($obj->working_date)); ?></td>
                    <td><?php echo $obj->work_status; ?></td>                    
                    <td> 
                        <select name="work_priority" id="<?php echo $obj->id; ?>" onchange="changePriority(this.id, this.value);">
                            <option value="1" <?php if($obj->work_priority == 1){ echo 'selected="selected"';} ?>>Urgent</option>
                            <option value="2" <?php if($obj->work_priority == 2){ echo 'selected="selected"';} ?>>Important</option>
                            <option value="3" <?php if($obj->work_priority == 3){ echo 'selected="selected"';} ?>>As Usual</option>
                        </select>
                    </td>                    
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="10"> No task record found.</td>
                </tr>
                <?php endif; ?>
            </table>
             <div class="paging paging-right"><?php echo $pagination; ?></div>
        </div>                
    </div>   
</div>

<script type="text/javascript">
    
 function changePriority(id, value)
 {      
     if(!confirm('Are you sure you want to cheange task priority?'))
     {
         return false;
     }
     
     $.ajax({
        type:"POST",
        data: {task_id:id, priority:value},
        async:false,
        url:"<?php echo site_url(); ?>admin/employee/priority",
        success:function(){
           location.reload();
        }
    });
 }
 
</script>

