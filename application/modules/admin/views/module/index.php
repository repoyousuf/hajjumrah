<div class="span12">
     <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span3">Modules</div>
            <div class="span6 filter-form">
                <?php echo form_open_multipart('admin/module/index', '', ''); ?>
                Select Project: 
                <select id="project_id" name="project_id" onchange="this.form.submit();">
                    <option value="">--Select--</option>
                   <?php foreach ($projects AS $obj): ?>
                        <option value="<?php echo $obj->id; ?>" <?php if($project_id == $obj->id){ echo 'selected="selected"';} ?>>(<?php echo $obj->project_no; ?>) <?php echo $obj->project_name; ?></option>                                        
                    <?php endforeach; ?>
                </select>
               <?php echo form_close(); ?> 
            </div>
            <div class="span3"><a class="add-new" href="<?php echo site_url('admin/module/add'); ?>">Add New</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <table width="100%" border="1" class="table-grid" >
                <tr>
                    <th>#SL</th>
                    <th>Project No</th>
                    <th>Project Name</th>
                    <th>Module Name</th>
                    <th>Description</th>
                    <th>Deadline</th>
                    <th>Assign Status</th>
                    <th>Work Status</th>
                    <th>Status</th>                    
                    <th>Action</th>
                </tr>
                <?php $count = $count + 1; if(isset ($modules) && !empty ($modules)): ?>
                <?php foreach($modules as $obj ) : ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $obj->project_no; ?></td>
                    <td><?php echo $obj->project_name; ?></td>
                    <td><?php echo $obj->module_name; ?></td>
                    <td><?php echo $obj->description; ?></td>
                    <td><?php echo date('M j, Y', strtotime($obj->deadline)); ?></td>
                    <td><?php echo $obj->assign_status == 1? 'Assigned' : 'Not Assign'; ?></td>
                    <td><?php echo $obj->work_status; ?></td>
                    <td><?php echo $obj->status == 1 ? 'Active' : 'Inactive'; ?></td>
                    <td>
                        <a href="<?php echo site_url('admin/module/edit/'.$obj->id); ?>">Edit</a> |
                        <a href="<?php echo site_url('admin/module/delete/'.$obj->id); ?>" onclick="javascript: return confirm('Are you sure you want to delete this module?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="10"> No module record found.</td>
                </tr>
                <?php endif; ?>
            </table>
             <div class="paging paging-right"><?php echo $pagination; ?></div>
        </div>                
    </div>   
</div>

