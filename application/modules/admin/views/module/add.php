<div class="span12">
      <style type="text/css">
        .table-form tr th {width: 25%;}
    </style>
<link type="text/css" href="<?php echo base_url(); ?>datepicker/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url(); ?>datepicker/ui/ui.datepicker.js"></script>

 <script type="text/javascript">
        $(function() {
         $("#deadline").datepicker({changeMonth: true,changeYear: true,showOn: 'both', buttonImage: '<?php echo base_url(); ?>assets/images/calendar.gif', buttonImageOnly: true});
      });        
  </script>    
    <div class="row-fluid"> 
        <?php $this->load->view('layout/message'); ?> 
    </div>    
    <div class="row-fluid"> 
        <div class="span12 home-box-title">
            <div class="span6">Add Module </div>
            <div class="span6"><a class="add-new" href="<?php echo site_url('admin/module/index'); ?>">Modules</a></div>
        </div>
    </div>
    <div class="row-fluid"> 
        <div class="span12 body-content">
            <?php
                $form_action = 'admin/module/add';
                $hidden = array();
                if(isset($module) && !empty ($module))
                {
                    $form_action = 'admin/module/edit';
                    $hidden['id'] = $module->id;
                }
            ?>
            <?php echo form_open_multipart($form_action, '', $hidden); ?>
            <table width="100%" border="0" class="table-form feedback" >
                <tr>
                    <th width="25%"></th>
                    <td width="50%">Fields with <span class="required">*</span> are required.</td>
                    <td width="25%"></td>
                </tr>
                <tr><td>&nbsp;</td><td colspan="2" ><div class="fn-duplicate-image"></div></td></tr>                
               <tr>
                    <th><label for="project_id">Project Name : <span class="required">*</span></label></th>
                    <td>
                        <select id="project_id" name="project_id" style="width:auto;" onchange="getModuleList(this.value);">
                            <option value="">--Select--</option>
                            <?php foreach ($projects AS $obj): ?>
                                <option value="<?php echo $obj->id; ?>" <?php if( @$module->project_id == $obj->id){ echo 'selected="selected"';} ?> >(<?php echo $obj->project_no; ?>) <?php echo $obj->project_name; ?></option>                                        
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td><?php echo form_error('full_name'); ?></td>
               </tr>
               <tr>
                    <th><label for="module_name">Module Name : <span class="required">*</span></label></th>
                    <td><input type="text" name="module_name" id="module_name" class="required"  value="<?php echo set_value('module_name', isset($module) && isset($module->module_name) ? $module->module_name : ''); ?>" /></td>
                    <td><?php echo form_error('module_name'); ?></td>
                </tr>                
                <tr>
                    <th><label for="note">Module Description : <span class="required">*</span></label></th>
                    <td><textarea  name="description" id="description" class="required" style="height: 100px;"><?php echo set_value('description', isset($module) && isset($module->description) ? $module->description : ''); ?></textarea></td>
                    <td><?php echo form_error('description'); ?></td>
                </tr>               
                <tr>
                    <th><label for="deadline">Deadline : <span class="required">*</span></label></th>
                    <td><input type="text" name="deadline" id="deadline" class="required"  value="<?php echo set_value('deadline', isset($module) && isset($module->deadline) ? $module->deadline : ''); ?>" /></td>
                    <td><?php echo form_error('deadline'); ?></td>
                </tr> 
              
                <?php if(isset($module) && !empty ($module)): ?>
                 <tr>
                    <th><label for="status">Status:<span class="required">*</span></label></th>
                    <td>
                        <select id="status" name="status" style="width:auto;">
                            <option value="">--Select--</option>
                            <option value="1" <?php echo set_select('status', '1', $module->status == 1 ? TRUE : FALSE); ?>>Active</option>                                        
                            <option value="0" <?php echo set_select('status', '0', $module->status == 0 ? TRUE : FALSE); ?>>Inactive</option>                                        
                        </select>
                    </td>
                    <td><?php echo form_error('status'); ?></td>
                </tr>
                 <?php endif; ?>
                <tr>
                    <th>&nbsp;</th>
                    <td>
                       <input  type="submit" class="btn btn-primary" id="submit"  name="submit" value="Save"/>
                       <input type="reset" class="btn btn-primary" id="reset"  name="reset" value="Reset"/>
                       <input type="button" class="btn btn-primary"  id="cancel" name="cancel" value="Cancel" onclick="window.location.href='<?php echo site_url('admin/project/index'); ?>'" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <?php echo form_close(); ?>   
        </div>                
    </div>   
</div>