<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to Hajj Umrah Admin Panel</title>
        <style type="text/css">
            body {
                background-color: #fff;
                margin: 40px;
                font: 13px/20px normal Helvetica, Arial, sans-serif;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 19px;
                font-weight: normal;
                margin: 0 0 14px 0;
                padding: 14px 15px 10px 15px;
            }

            code {
                font-family: Consolas, Monaco, Courier New, Courier, monospace;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            #body{
                margin: 0 15px 0 15px;
            }

            p.footer{
                text-align: right;
                font-size: 11px;
                border-top: 1px solid #D0D0D0;
                line-height: 32px;
                padding: 0 10px 0 10px;
                margin: 20px 0 0 0;
            }

            #container{
                margin: 10px;
                border: 1px solid #D0D0D0;
                -webkit-box-shadow: 0 0 8px #D0D0D0;
                width: 400px;
                text-align: left;
            }
        </style>
    </head>
    <body>
       <center> 
        <div id="container">
            <h1>Welcome to Hajj Umrah Admin Login Panel</h1>

            <div id="body">
                <p>If you would like login to admin panel please put the correct user and password in following fields.</p>
                <p></p>
                <code>
                    <?php echo form_open(site_url('admin/login'), array('name'=>'login','id'=>'login'), ''); ?>
                    <p><label>Username</label> <input type="text" name="username" id="username" value="" /></p>
                    <p><label>Password</label> <input type="text" name="password" id="password" value="" /></p>
                    <p><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label> <input type="submit" name="submit" id="submit" value="Login" /></p>
                    <?php echo form_close(); ?>
                </code>

                <p>Forgot your password? <a href="<?php echo base_url('admin/forgot'); ?>">Click Here</a>.</p>
                <p>Go Site Home <a href="<?php echo base_url(); ?>">Click Here</a>.</p>
            </div>

            <p class="footer">©2012-<?php echo date('Y'); ?> hajjomrah.com All rights reserved.</p>
        </div>
       </center>
    </body>
</html>