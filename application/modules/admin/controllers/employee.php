<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!logged_in_admin_id())
        {
            redirect();
            exit;
        }
        
        $this->load->model('admin_model', 'admin', true);
    }
    
    public function index()
    {        
        $admin_id = logged_in_admin_id();
        $offset = intval($this->uri->segment(4));
        $offset = $offset ? $offset : 0;

        $result = $this->admin->get_paged_list('employees', array(), $url = 'admin/employee/index/', $segment = 4, $offset);
       
        if(empty ($result))
        {
            redirect('admin/employee/add');
        }
        else
        {     
            $data['count'] = $offset;
            $data['employees'] = $result['rows'];
            $data['pagination'] = $result['pagination'];
            $this->layout->title('TMS :: Employees');
            $this->layout->view('employee/index', $data);
        } 
    }    

    
    public function add()
    {    
       $data = array();     
        
        if($_POST)
        {
            $this->_prepare_employee_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_employee_data();
                $insert_id = $this->admin->insert('employees', $data);
                if($insert_id)
                {
                    success(INSERT_SUCCESS);
                    redirect('admin/employee/index');
                }
                else
                {
                    error(INSERT_FAILED);
                    redirect('admin/employee/add');
                }
            }
            else
            {
                $data = $_POST;
            }
         }
         
         $this->layout->title('TMS :: Add Employee');
         $this->layout->view('employee/add', $data);
    }
    
    public function edit()
    {
        $employee_id = $this->uri->segment(4);
        $data = array();
        
        if($_POST)
        {
            $this->_prepare_employee_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_employee_data();
                $updated = $this->admin->update('employees', $data, array('id'=>$this->input->post('id')));
               
                if($updated)
                {
                    success(UPDATE_SUCCESS);
                    redirect('admin/employee/index');
                    exit;
                }
                else
                {
                    error(UPDATE_FAILED);
                    redirect('admin/employee/edit/'.$this->input->post('id'));
                }
            }
            else
            {
               redirect('admin/employee/edit/'.$this->input->post('id'));          
            }
        }
        
        if ($employee_id) {
            $employee = $this->admin->get_single('employees', array('id' => $employee_id));

            if (!$employee) {
                redirect('admin/employee/add');
            }
        }
       
        
         $data['employee'] = $employee;
         $this->layout->title('TMS :: Edit Employee');
         $this->layout->view('employee/add', $data);
    }
    
    private function _prepare_employee_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error-message" style="display:inline-block;">', '</div>');

        $this->form_validation->set_rules('user_type', 'user_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('full_name', 'full_name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('present_address', 'present_address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('permanent_address', 'permanent_address', 'trim|required|xss_clean');
        $this->form_validation->set_rules('contact_number', 'contact_number', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email_addresses', 'email_addresses', 'trim|required|xss_clean');
        $this->form_validation->set_rules('url', 'url', 'trim|required|xss_clean');
        $this->form_validation->set_rules('facebook', 'facebook', 'trim|required|xss_clean');
        $this->form_validation->set_rules('messenger', 'messenger', 'trim|required|xss_clean');
        $this->form_validation->set_rules('education', 'education', 'trim|required|xss_clean');
        $this->form_validation->set_rules('experience', 'experience', 'trim|required|xss_clean');
        $this->form_validation->set_rules('salary', 'salary', 'trim|required|xss_clean');
        $this->form_validation->set_rules('employment_date', 'employment_date', 'trim|required|xss_clean');
        $this->form_validation->set_rules('job_type', 'job_type', 'trim|required|xss_clean');
         if(!$this->input->post('id'))
         {
            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
         }
    }
    
    private function _get_posted_employee_data()
    {
        $items = array();
       
        $items[] = 'user_type';         
        $items[] = 'full_name';         
        $items[] = 'present_address';         
        $items[] = 'permanent_address';  
        $items[] = 'contact_number';  
        $items[] = 'email_addresses';  
        $items[] = 'url';  
        $items[] = 'facebook';  
        $items[] = 'messenger';  
        $items[] = 'education';  
        $items[] = 'experience';  
        $items[] = 'salary';  
        $items[] = 'job_type';  
        
        $data = elements($items, $_POST); 
       
        //upload  resume
       
        if($_FILES['resume']['name'] != '')
        {
            $upload_array['files_array'] = $_FILES['resume'];
            $upload_array['image_prefix'] = uniqid() . '_';

            if($this->input->post('member_id'))
            {
                $upload_array['redirect_url'] = 'admin/empployee/edit';
            }
            else
            {
                $upload_array['redirect_url'] = 'admin/employee/add';
            }

            $this->load->library('resume_upload');
            $resume_name = $this->resume_upload->process($upload_array);

            $this->admin->delete_file('images/companies/' . $this->input->post('prev_resume'));

            $data['resume'] = $resume_name;
        }
        
        // upload photo
        if($_FILES['photo']['name'] != '')
        {
            $upload_array['files_array'] = $_FILES['photo'];
            $upload_array['image_prefix'] = uniqid() . '_';

            if($this->input->post('id'))
            {
                $upload_array['redirect_url'] = 'admin/employee/edit';
            }
            else
            {
                $upload_array['redirect_url'] = 'job/employee/add';
            }

            $this->load->library('photo_upload');
            $photo_name = $this->photo_upload->process($upload_array);

            $this->admin->delete_file('assets/images/photos/' . $this->input->post('prev_photo'));

            $data['photo'] = $photo_name;
        }        
        
        $data['employment_date'] = date('Y-m-d', strtotime($this->input->post('employment_date')));
       
        

        if($this->input->post('id'))
        {
            $data['status'] = $this->input->post('status');
            $data['modified'] = date('Y-m-d H:i:s');
            $data['modified_by'] = $this->session->userdata('login_id');
        }
        else
        {
            $data['status'] = 1;
            $data['created'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata('login_id');
            $data['password'] = md5($this->input->post('password'));
            $data['email'] = $this->input->post('email');
        }
        return $data;
    }
    
    
    public function details()
    {
        $employee_id = $this->uri->segment(4);
        $data = array();

        if ($employee_id) {
            $employee = $this->admin->get_single('employees', array('id' => $employee_id));

            if (!$employee) {
                redirect('admin/employee/index');
            }
        }
        
        $data['employee'] = $employee;
        $this->layout->title('TMS :: Edit Employee');
        $this->layout->view('employee/details', $data);
    }
    
    public function delete($id)
    {
        if($this->admin->delete('projects', array('id' => $id)))
        {
            success(DELETE_SUCCESS);
        }
        else
        {
            error(DELETE_FAILED);
        }
        redirect('admin/project/index');
    }
    
    
    public function tasks()
    {
        $task_type = $this->uri->segment(4);        
        $offset = intval($this->uri->segment(4));
        $offset = $offset ? $offset : 0;
        
        
        
        $emp_id     = $this->input->post('employee_id');
        $start_date = date('Y-m-d', strtotime($this->input->post('start_date')));
        $end_date   = date('Y-m-d', strtotime($this->input->post('end_date')));
        
        if($this->input->post('employee_id'))
        {
            $this->session->set_userdata('emp_id', $emp_id);
        }
        else{
           $emp_id = $this->session->userdata('emp_id');
        }
        
        if($this->input->post('start_date') && $this->input->post('end_date'))
        {
            $this->session->set_userdata('start_date', $start_date);
            $this->session->set_userdata('end_date', $end_date);
        }
        else{
           $start_date = $this->session->userdata('start_date');
           $end_date = $this->session->userdata('end_date');
        }
          
        
        $result = $this->admin->get_paged_task_list( $url = 'admin/employee/tasks/', $segment = 4, $offset, $task_type, $emp_id, $start_date, $end_date);
        
        $data['employees'] = $this->admin->get_list('employees', array('status' => 1), "id,full_name"); 
        
         
         
        $data['count'] = $offset;
        $data['tasks'] = $result['rows'];
        $data['label'] = ucwords(str_replace('-', " ", $task_type));
        $data['pagination'] = $result['pagination'];
        $data['emp_id'] = $emp_id;
        $data['start_date'] = $start_date;
        $data['end_date'] = $end_date;
        
        $this->layout->title('TMS :: Task List');
        $this->layout->view('employee/tasks', $data);
    }
    
    
    public function priority()
    {
       $data['work_priority'] = $this->input->post('priority');
       $data['modified']     = date('Y-m-d H:i:s');
       $data['modified_by']     = logged_in_admin_id();
       echo $this->admin->update('tasks', $data, array('id'=>$this->input->post('task_id')));            
    }


    public function status()
    {
           $data['work_status'] = $this->input->post('status');
           $data['modified']     = date('Y-m-d H:i:s');
           $data['modified_by']     = logged_in_admin_id();
           $this->admin->update('tasks', $data, array('id'=>$this->input->post('id'))); 
           redirect('admin/employee/detail_task/'.$this->input->post('id'));
    }
    
    
    public function profile()
    {
         $employee = $this->admin->get_single('employees', array('id' => logged_in_admin_id()));
        
         $data['employee'] = $employee;
         $this->layout->title('TMS :: Employee Info');
         $this->layout->view('employee/profile', $data);
    }




    /* ajax call */
    public function get_category()
    {
        $company_id = $this->input->post('company_id');
        $cat_id = $this->input->post('cat_id');
        
        
        $categories = $this->apps->get_list('apps_categories', array('status' => 1, 'apps_company_id'=>$company_id)); 
        $str =  '<option value="" >--Select--</option>';
        if(!empty ($categories))
        {     
            $select = '';
            foreach($categories as $obj )
            {   
                $select = isset ($cat_id) && $obj->id == $cat_id ? 'selected="selected"' : '';
                $str .= '<option value="'.$obj->id.'" '.$select.'> '.$obj->apps_category.'</option>';
            }
        }
        echo $str;
    }
    
    
     public function check_duplicate_apps()
    {
        $company_id = $this->input->post('com_id');
        $cat_id = $this->input->post('cat_id');
        $apps_name = $this->input->post('apps');
        
        
        $apps = $this->apps->get_single('apps', array('status' => 1, 'apps_company_id'=>$company_id,'apps_category_id'=>$cat_id, 'apps_name'=>$apps_name)); 
        $str =  '';
        if(!empty ($apps))
        {     
           $str =  '<img src="'.$apps->apps_image_path.'" alt="" width="150" style="padding;3px;" />';
        }
        
        echo $str;
    }
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */