<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Module extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        if(!logged_in_admin_id())
        {
            redirect();
            exit;
        }
        
        $this->load->model('admin_model', 'admin', true);
    }
    
    public function index()
    {        
        $admin_id = logged_in_admin_id();
        $offset = intval($this->uri->segment(4));
        $offset = $offset ? $offset : 0;
        
        $project_id = '';
        
        if($_POST)
        {
            $project_id = $this->input->post('project_id');
            $this->session->set_userdata('project_id',$project_id);
        }
        else if($this->session->userdata('project_id'))
        {
            $project_id = $this->session->userdata('project_id');
        }
        
        
        
        $result = $this->admin->get_paged_list_module( $url = 'admin/module/index/', $segment = 4, $offset, $project_id );

       
        if(empty ($result))
        {
            redirect('admin/module/add');
        }
        else
        {     
            $data['projects'] = $this->admin->get_list('projects', array('status' => 1)); 
            $data['count'] = $offset;
            $data['modules'] = $result['rows'];
            $data['project_id'] = $project_id;
            $data['pagination'] = $result['pagination'];
            $this->layout->title('TMS :: Modules');
            $this->layout->view('module/index', $data);
        } 
    }    

    
    public function add()
    {    
       $data = array();     
        
        if($_POST)
        {
            $this->_prepare_module_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_module_data();
                $insert_id = $this->admin->insert('modules', $data);
                if($insert_id)
                {
                    success(INSERT_SUCCESS);
                    redirect('admin/module/index');
                }
                else
                {
                    error(INSERT_FAILED);
                    redirect('admin/module/add');
                }
            }
            else
            {
                $data = $_POST;
            }
         }
         
         $data['projects'] = $this->admin->get_list('projects', array('status' => 1)); 
         $this->layout->title('TMS :: Add Module');
         $this->layout->view('module/add', $data);
    }
    
    public function edit()
    {
        $module_id = $this->uri->segment(4);
        $data = array();
        
        if($_POST)
        {
            $this->_prepare_module_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_module_data();
                $updated = $this->admin->update('modules', $data, array('id'=>$this->input->post('id')));
                
                if($updated)
                {
                    success(UPDATE_SUCCESS);
                    redirect('admin/module/index');
                    exit;
                }
                else
                {
                    error(UPDATE_FAILED);
                    redirect('admin/module/edit/'.$this->input->post('id'));
                }
            }
            else
            {
                $data = $_POST;             
            }
        }
        else
        {
            if ($module_id) {
                $module = $this->admin->get_single('modules', array('id' => $module_id));

                if (!$module) {
                    redirect('admin/module/add');
                }
            }
        }
        
         $data['module'] = $module;
         $data['projects'] = $this->admin->get_list('projects', array('status' => 1)); 
         $this->layout->title('TMS :: Edit Module');
         $this->layout->view('module/add', $data);
    }
    
    private function _prepare_module_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error-message" style="display:inline-block;">', '</div>');

        $this->form_validation->set_rules('project_id', 'project_id', 'trim|required|xss_clean');
        $this->form_validation->set_rules('module_name', 'module_name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'trim|required|xss_clean');
        $this->form_validation->set_rules('deadline', 'deadline', 'trim|required|xss_clean');     
    }
    
    private function _get_posted_module_data()
    {
        $items = array();
       
        $items[] = 'project_id';         
        $items[] = 'module_name';         
        $items[] = 'description';  
        $items[] = 'deadline';  
        
        $data = elements($items, $_POST); 
        $data['deadline'] = date('Y-m-d', strtotime($this->input->post('deadline')));        

        if($this->input->post('id'))
        {
            $data['status'] = $this->input->post('status');
            $data['modified'] = date('Y-m-d H:i:s');
            $data['modified_by'] = $this->session->userdata('login_id');
        }
        else
        {
            $data['status'] = 1;
            $data['created'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata('login_id');
        }
        return $data;
    }
    
    public function delete($id)
    {
        if($this->admin->delete('modules', array('id' => $id)))
        {
            success(DELETE_SUCCESS);
        }
        else
        {
            error(DELETE_FAILED);
        }
        redirect('admin/module/index');
    }
    
    
       
    /* ajax call */
    public function getModuleList( $insert_id = null )
    {
        $project_id = $this->input->post('project_id');        
        $module_id = $this->input->post('module_id');        
        
        $modules = $this->admin->get_list('modules', array('status' => 1, 'project_id'=>$project_id)); 
        $str =  '<option value="" >--All--</option>';
        if(!empty ($modules))
        {     
            $select = '';
            foreach($modules as $obj )
            {
                if($insert_id != '')
                 $select = isset ($insert_id) && $obj->id == $insert_id ? 'selected="selected"' : '';
                if($module_id != '')
                  $select = isset ($module_id) && $obj->id == $module_id ? 'selected="selected"' : '';
                
                $str .= '<option value="'.$obj->id.'" '.$select.'> '.$obj->module_name.'</option>';
            }
        }
        
        echo $str;
    }
    
    public function getTaskList( )
    {
        $employee_id = $this->input->post('employee_id');        
        $project_id = $this->input->post('project_id');        
        $module_id = $this->input->post('module_id');        
        $task_id = $this->input->post('task_id'); 
        
        $condition = array(
            'status'=>1,
        );
        
        if($employee_id)
        {
            $condition['employee_id'] = $employee_id;
        }
        if($project_id)
        {
            $condition['project_id'] = $project_id;
        }
        if($module_id)
        {
            $condition['module_id'] = $module_id;
        }
        if($task_id)
        {
            $condition['task_id'] = $task_id;
        }
        
        $tasks = $this->admin->get_list('tasks', $condition); 
        $str =  '<option value="" >--All--</option>';
        if(!empty ($tasks))
        {     
            $select = '';
            foreach($tasks as $obj )
            {   
                $select = isset ($task_id) && $obj->id == $task_id ? 'selected="selected"' : '';
                $str .= '<option value="'.$obj->id.'" '.$select.'> '.$obj->tasks.'</option>';
            }
        }
        
        echo $str;
    }
    
    
    public function addModule()
    {
        $data['project_id'] = $this->input->post('project_id'); 
        $data['module_name'] = $this->input->post('module_name'); 
        $data['description'] = $this->input->post('description'); 
        $data['deadline'] =  date('Y-m-d', strtotime($this->input->post('deadline'))); 
        $data['status'] = 1;
        $data['created'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata('login_id');
        
        $insert_id = $this->admin->insert('modules', $data);
        
        $this->getModuleList($insert_id);
    }
    
    
   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */