<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Metadata extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(!logged_in_admin_id())
        {
            redirect();
            exit;
        }
        $this->load->model('metadata_model', 'metadata', true);
    }

    public function index()
    {
        $admin_id = logged_in_admin_id();
        $offset = intval($this->uri->segment(4));
        $offset = $offset ? $offset : 0;        
        
        $result = $this->metadata->get_paged_list_metadata( $url = 'admin/metadata/index/', $segment = 4, $offset );
       
        if(empty ($result))
        {
            redirect('admin/metadata/add');
        }
        else
        {     
            $data['count'] = $offset;
            $data['metadata'] = $result['rows'];           
            $data['pagination'] = $result['pagination'];
            $this->layout->title('BD_APPS | Meta Data');
            $this->layout->view('metadata', $data);
        }        
    }
    
    public function add()
    {    
       $data = array();     
        
        if($_POST)
        {
            $this->_prepare_metadata_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_metadata();
                $insert_id = $this->metadata->insert('meta_data', $data);
                if($insert_id)
                {
                    success(INSERT_SUCCESS);
                    redirect('admin/metadata/index');
                }
                else
                {
                    error(INSERT_FAILED);
                    redirect('admin/metadata/add');
                }
            }
            else
            {
                $data = $_POST;
            }
         }
         
         $this->layout->title('BD-APPS | Add Metadata');
         $this->layout->view('add_metadata', $data);
    }
    
    public function edit()
    {
        $metadata_id = $this->uri->segment(4);
        $data = array();
        
        if($_POST)
        {
            $this->_prepare_metadata_validation();
            if($this->form_validation->run() === TRUE)
            {
                $data = $this->_get_posted_metadata();
                $updated = $this->metadata->update('meta_data', $data, array('id'=>$this->input->post('id')));
                
                if($updated)
                {
                    success(UPDATE_SUCCESS);
                    redirect('admin/metadata/index');
                    exit;
                }
                else
                {
                    error(UPDATE_FAILED);
                    redirect('admin/metadata/edit/'.$this->input->post('id'));
                }
            }
            else
            {
                $data = $_POST;             
            }
        }
        else
        {
            if ($metadata_id) {
                $metadata = $this->metadata->get_single('meta_data', array('id' => $metadata_id));

                if (!$metadata) {
                    redirect('admin/metadata/add');
                }
            }
        }
        
         $data['metadata'] = $metadata;
         $this->layout->title('BD-APPS | Edit Metadata');
         $this->layout->view('add_metadata', $data);
    }
    
    private function _prepare_metadata_validation()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error-message" style="display:inline-block;">', '</div>');

        $this->form_validation->set_rules('page_name_slug', 'Meta Data Page', 'trim|required|xss_clean');
        $this->form_validation->set_rules('meta_keyword', 'Meta Keyword', 'trim|required|xss_clean');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim|required|xss_clean');
    }
    
    private function _get_posted_metadata()
    {
        $items = array();
       
        $items[] = 'page_name_slug';
        $items[] = 'meta_keyword';         
        $items[] = 'meta_description';
        
        $data = elements($items, $_POST);

        if($this->input->post('id'))
        {
            $data['modified'] = date('Y-m-d H:i:s');
            $data['modified_by'] = $this->session->userdata('admin');
        }
        else
        {
            $data['created'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata('admin');
        }

        return $data;
    }
    
    public function delete($id)
    {
        if($this->metadata->delete('metadata', array('id' => $id)))
        {
            success(DELETE_SUCCESS);
        }
        else
        {
            error(DELETE_FAILED);
        }
        redirect('admin/metadata/index');
    }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */