<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  function __construct()
    {
        parent::__construct();
        if(!logged_in_admin_id())
        {
            redirect();
            exit;
        }
    }
    public function index()
    {        
        $data = array();
        $this->layout->title('');
        $this->layout->view('dashboard', $data); 
    }      
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */