<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model', 'admin', true);
    }

    public function index() {
        $this->load->helper('form');
        $data = array();
        $this->load->view('index', $data);
    }

    public function login() {
        if ($_POST) {
            $data['username'] = $this->input->post('username');
            $data['password'] = md5($this->input->post('password'));

            $admin_info = $this->admin->get_single('ho_admin', $data);
           
            if ($admin_info) {
                foreach ($admin_info as $key => $value) {
                    $this->session->set_userdata($key, $value);
                }

                success('You have successfully logged In.');
                redirect('admin/dashboard');
            } else {
                error('Invalid user OR password. Please try again later.');
                redirect('admin');
            }
        }

        redirect('admin');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('admin');
        exit;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */