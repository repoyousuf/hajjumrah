<?php
if(!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Metadata_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
    // get data with paging
    function get_paged_list_metadata( $url, $segment, $offset = 0 )
    {
        $result = array('rows' => array(), 'total_rows' => 0);
        $this->load->library('pagination');
        $limit = $this->config->item('admin_per_page');
        
        $sql = "SELECT MD.* 
                FROM meta_data AS MD
                ";       
        
        $sql_rows = $sql . " ORDER BY MD.id ASC LIMIT $offset, $limit";
        $result['rows'] = $this->db->query($sql_rows)->result();
      
        
        $result['total_rows'] = $total_rows = count($this->db->query($sql)->result());

        $config['uri_segment'] = $segment;
        $config['base_url'] = site_url() . $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('admin_per_page');

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }

}

?>
