<?php
if(!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Admin_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
    }
    
        // get data with paging
    function get_paged_list_module( $url, $segment, $offset = 0, $project_id = null)
    {
        $result = array('rows' => array(), 'total_rows' => 0);
        $this->load->library('pagination');
        $limit = $this->config->item('admin_per_page');
        
        $sql = "SELECT P.project_no, P.project_name, M.* 
                FROM modules AS M
                LEFT JOIN projects AS P ON P.id = M.project_id                
                ";
        if($project_id)
            $sql .= " WHERE M.project_id = $project_id ";
        
        $sql_rows = $sql . " ORDER BY M.status DESC, M.created DESC LIMIT $offset, $limit";
        $result['rows'] = $this->db->query($sql_rows)->result();
      
        
        $result['total_rows'] = $total_rows = count($this->db->query($sql)->result());

        $config['uri_segment'] = $segment;
        $config['base_url'] = site_url() . $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('admin_per_page');

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }
    
        // get data with paging
    function get_paged_list_roster( $url, $segment, $offset = 0, $employee_id = null)
    {
        $result = array('rows' => array(), 'total_rows' => 0);
        $this->load->library('pagination');
        $limit = $this->config->item('admin_per_page');
        
        $sql = "SELECT E.full_name, R.* 
                FROM rosters AS R
                LEFT JOIN employees AS E ON E.id = R.employee_id                
                ";
        if($employee_id)
            $sql .= " WHERE R.employee_id = $employee_id ";
        
        $sql_rows = $sql . " ORDER BY R.status DESC, R.created DESC LIMIT $offset, $limit";
        $result['rows'] = $this->db->query($sql_rows)->result();
      
        
        $result['total_rows'] = $total_rows = count($this->db->query($sql)->result());

        $config['uri_segment'] = $segment;
        $config['base_url'] = site_url() . $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('admin_per_page');

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }

    function get_paged_task_list( $url, $segment, $offset = 0, $task_type = null, $emp_id = null, $start_date = null, $end_date = null )
    {
        $result = array('rows' => array(), 'total_rows' => 0);
        $this->load->library('pagination');
        $limit = $this->config->item('admin_per_page');
        
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id               
                LEFT JOIN employees AS E ON E.id = T.employee_id                 
                ";
        
        $whehe = $emp_id ?  "WHERE T.employee_id = $emp_id AND" : " WHERE ";
        
        if($task_type == 'completed-task')
            $sql .= "$whehe T.work_status = 'Finish' || T.work_status = 'Complete'";
        if($task_type == 'overdue-task')
            $sql .= " $whehe T.work_status != 'Finish' AND T.work_status != 'Complete' AND T.working_date < '".date('Y-m-d')."'";
        if($task_type == 'todays-task')
            $sql .= " $whehe T.work_status != 'Finish' AND T.work_status != 'Complete' AND T.working_date = '".date('Y-m-d')."'";
        if($task_type == 'future-task')
            $sql .= " $whehe T.work_status != 'Finish' AND T.work_status != 'Complete' AND T.working_date >'".date('Y-m-d')."'";
        
        if($start_date && $end_date)
            $sql .= " $whehe T.working_date BETWEEN '$start_date' AND '$end_date' ";
        
        $sql_rows = $sql . " ORDER BY T.created DESC LIMIT $offset, $limit";
        $result['rows'] = $this->db->query($sql_rows)->result();
      
        
        $result['total_rows'] = $total_rows = count($this->db->query($sql)->result());

        $config['uri_segment'] = $segment;
        $config['base_url'] = site_url() . $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('admin_per_page');

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }
    
    function get_task( $task_id )
    {
                
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id                 
                LEFT JOIN employees AS E ON E.id = T.employee_id 
                WHERE T.id = $task_id
                ";
       
        $result = $this->db->query($sql)->row();  
        
        
        return $result;
    }
    
    
    public function get_performence_report($start_date, $end_date, $employee_id, $project_id, $module_id, $task_id )
    {
        
        $sql = "SELECT P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN projects AS P ON P.id = T.project_id                
                LEFT JOIN modules AS M ON M.id = T.module_id                
                WHERE T.status = 1 ";
        if($start_date != '' && $end_date != '')
            $sql .= " AND T.working_date BETWEEN '$start_date' AND '$end_date' ";
        
        if($employee_id)
            $sql .= " AND T.employee_id = $employee_id ";
        if($project_id)
            $sql .= " AND T.project_id = $project_id ";
        if($module_id)
            $sql .= " AND T.module_id = $module_id ";
        if($task_id)
            $sql .= " AND T.id = $task_id ";
        
        return $this->db->query($sql)->result();          
    }
    
    public function get_inccomplete_testing( $task_id)
    {
        if(!$task_id)  return;
        
        $sql = "SELECT * FROM testing WHERE task_id = $task_id AND (work_status = 'New' OR work_status = 'Progress') ";
        return $this->db->query($sql)->result(); 
    }
    
    public function get_consume_times_today()
    {
        
       $sql = "SELECT CT.* , SUM( CT.consume_hour ) AS consume_total_hour, 
                P.project_no, P.project_name, M.module_name, T.tasks, T.working_date, T.work_status, T.hour_per_day
                FROM  consume_times  AS CT
                LEFT JOIN tasks AS T ON T.id = CT.task_id                
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id  
                WHERE CT.consume_date = '".date('Y-m-d')."'
                GROUP BY CT.task_id 
                ORDER BY T.working_date ASC";
        return $this->db->query($sql)->result(); 
    }
    
    public function get_consume_times()
    {

      $seven_days =date ( 'Y-m-d', strtotime ( '-7 day' . date('Y-m-d'))); 

       $sql = "SELECT CT.* , SUM( CT.consume_hour ) AS consume_total_hour, 
                P.project_no, P.project_name, M.module_name, T.tasks, T.working_date, T.work_status, T.hour_per_day
                FROM  consume_times  AS CT
                LEFT JOIN tasks AS T ON T.id = CT.task_id                
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id  
                 WHERE CT.consume_date > '$seven_days' AND CT.consume_date < '".date('Y-m-d')."'
                GROUP BY CT.task_id ORDER BY CT.consume_date DESC";
        return $this->db->query($sql)->result(); 
    }
    
    
    public function get_todays_task($emp_id)
    {
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id               
                LEFT JOIN employees AS E ON E.id = T.employee_id                 
                WHERE T.employee_id = $emp_id AND (T.work_status != 'Finish' || T.work_status != 'Complete') AND T.working_date = '".date('Y-m-d')."'";
        
        $sql = $sql . " ORDER BY T.created DESC";
        return $this->db->query($sql)->result();      
        
    }
    
    public function get_done_task($emp_id)
    {
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id               
                LEFT JOIN employees AS E ON E.id = T.employee_id                 
                WHERE T.employee_id = $emp_id AND (T.work_status = 'Finish' || T.work_status = 'Complete' ) ";
        
        $sql = $sql . " ORDER BY T.working_date ASC";
        return $this->db->query($sql)->result();      
        
    }
    
    public function get_due_task($emp_id)
    {
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id               
                LEFT JOIN employees AS E ON E.id = T.employee_id                 
                WHERE T.employee_id = $emp_id AND T.work_status != 'Finish' AND T.working_date < '".date('Y-m-d')."'";
        
        $sql = $sql . " ORDER BY T.working_date ASC";
        return $this->db->query($sql)->result();      
        
    }
    
    public function get_future_task($emp_id)
    {
        $sql = "SELECT E.full_name, P.project_no, P.project_name, M.module_name , T.*
                FROM tasks AS T
                LEFT JOIN modules AS M ON M.id = T.module_id                
                LEFT JOIN projects AS P ON P.id = M.project_id               
                LEFT JOIN employees AS E ON E.id = T.employee_id                 
                WHERE T.employee_id = $emp_id AND T.work_status != 'Finish' AND T.working_date > '".date('Y-m-d')."'";
        
        $sql = $sql . " ORDER BY T.working_date ASC";
        return $this->db->query($sql)->result();      
        
    }
    
    
}
?>
