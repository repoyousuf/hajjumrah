<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title_for_layout; ?></title>         
        <meta charset="utf-8"/>
        <link rel="canonical" href="<?php echo 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta property="locale" content="en_US" />        
        <meta property="type" content="Hajj & Umrah" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/hajjumrah.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url(); ?>assets/images/hajjumrah.ico" type="image/x-icon">
        <meta property="title" content="<?php echo $title_for_layout; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta property="url" content="<?php echo 'http://' . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; ?>" />
        <meta property="site_name" content="Hajj & Umrah" /> 
        <meta name="keywords" content="Hajj & umrah mobile application"/>
        <meta name="description" content="Personalized and Spatio-Temporal Hajj and Umrah Ritual Services"/>        
        
        <link href="<?php echo CSS_URL; ?>style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CSS_URL; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CSS_URL; ?>print.css" rel="stylesheet" media="print" type="text/css" />
        <link href="<?php echo CSS_URL; ?>parallax-slider.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CSS_URL; ?>jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CSS_URL; ?>hajj-custom-screen.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo CSS_URL; ?>custom-css.css" rel="stylesheet" type="text/css" />
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js" type="text/javascript" ></script>
    </head>
    <body>       
        <!-- header-wrap -->         
       <?php echo $this->load->view('layout/header'); ?>
       <!-- header-wrap --> 
          
        <!-- Featured Slider DOM-->
        <?php if(isset ($activeTab) && ($activeTab == 'home' || $activeTab == 'whatweoffer' || $activeTab == 'features')){ ?>
       <?php echo $this->load->view('layout/featuredSlider'); ?>
        <?php } ?>
        
        
        <!-- content-wrap -->
        <div class="layout-main">
            <div class="content-wrap">
                <?php echo $this->load->view('layout/breadcrumb'); ?>
                
                <!-- Page Specific DOM-->
                <?php echo $content_for_layout; ?>
            </div>
        </div>
    
        
        <!-- footer -->
       <?php echo $this->load->view('layout/footer'); ?>
        <!-- footer -->
                        
      
        <script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js" type="text/javascript" ></script>
        <!-- JS Implementing Plugins -->
        <script src="<?php echo base_url(); ?>assets/js/flexslider-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.cslider.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/js/back-to-top.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL; ?>jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/apps.js"></script> 
        <script type="text/javascript">
            $(document).ready(function() {
                App.init();
                Index.initParallaxSlider();
                                                    
                $(".fn_email_exists").blur(function(){
                    var email  = $(this).val();

                    if( email != ""){
                        $.post("<?php echo site_url('site/duplicate_email_check'); ?>",{
                            email:email
                        },function(data){
                            if(data){
                                alert('This email already exists. Please try a new one.');
                                $('#email').val('');
                                return true;
                            }
                        });
                    }
                });
            });
        </script>
        
    </body>
</html>