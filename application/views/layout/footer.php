<div class="layout-main">
    <footer>
        <div class="container">
            <div class="row">
                <div class="twelve columns">
                    <div class="footer-content">
                        <ul class="footer-menu unstyled inline">
                            <li><a href="<?php echo site_url(); ?>">Home</a></li>
                            <li><a href="<?php echo site_url('site/whatweoffer'); ?>">What We Offer</a></li>
                            <li><a href="<?php echo site_url('site/download'); ?>">Download</a></li>         
                            <li><a href="<?php echo site_url('site/tutorials'); ?>">FAQ / Video Tutorial</a></li>         
                            <li><a href="<?php echo site_url('site/privacy'); ?>">Privacy & Policy</a></li>
                            <li><a href="<?php echo site_url('site/contactus'); ?>">Contact Us</a></li>
                            <li><a href="<?php echo site_url('site/webadmin'); ?>">Web Admin</a></li>
                            <li><a href="<?php echo site_url('site/subscribe'); ?>">Subscribe</a></li>
                            <li>
                                <a href="javascript:void(0);">Total visitors : 
                                <b>
                                    <?php echo  (isset($activeTab) && $activeTab == 'download') ? download_counter() : site_counter();  ?>
                                </b>
                                </a>
                            </li>
                        </ul>                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="twelve columns">
                    <p class="footer-text">Copyright 2012-<?php echo date('Y'); ?>. MediaLab, Umm Al Qura University.</p>
                </div>
            </div>
        </div>
    </footer>
</div>