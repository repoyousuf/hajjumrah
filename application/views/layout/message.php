 <?php if($this->session->flashdata('success') || $this->session->flashdata('error')){ ?>
    <div class="message-block">
        <?php if($this->session->flashdata('success')){ ?>
        <div class="success-msg"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
        <?php if($this->session->flashdata('error')){ ?>
        <div class="error-msg"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
    </div>
<?php } ?>
