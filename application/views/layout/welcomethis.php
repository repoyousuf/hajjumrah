<div id="divWelcomeThis" style="display: block;">
    <div id="fn_controll_device" class="divWelcomeBox" style="width: 500px; top: 100px; display: block;">
        <a href="javascript:void(0)" id="ancModalCloseIcon" class="ancModalToolbarX fn_close">
            <img src="<?php echo IMG_URL; ?>cross.png" alt="Close">
        </a>
        <div id="dvModalContent" class="dvModalContent">
            <div id="dvDialogTopPane" class="dvDialogTopPane">
                <h2>Welcome to "Family Connect Mobile Application"</h2>
            </div>
            <div id="dvDialogContent" class="dvDialogContent">
                <div>
                    <p style="font-style: italic;"><strong>Our Family Connect Mobile Application</strong> is coming soon with the following features. </p>
                    <div class="offerImage"><img alt="Family Connect" src="<?php echo IMG_URL; ?>family-connect-img.jpg"/></div>
                    <ul>
                        <li>
                            Friends and relatives can send friend request to Pilgrim’s.
                        </li>
                        <li>
                            They can share text, image and audio with each other through Hajj Messenger.
                        </li>
                        <li>
                            Friends and Relatives been notified, once pilgrim’s reach inside or outside of the Haram boundary, Arafat boundary, Mina boundary and Muzdalifah boundary.
                        </li>
                    </ul>                   
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="dvDialogBottomPane">
                <h4 style="font-style: italic;">Are you interested?  <strong>Visit us</strong> <small>later Please!</small></h4>
              
                <p><strong>Or</strong> 
                    <a href="<?php echo site_url('site/subscribe'); ?>" class="button btnSmall btnGrey" role="button">Subscribe Now</a> 
                    if you want us to let you know. <a href="javascript:void(0)" class="button btnSmall btnRed fn_close" role="button" >No Thanks</a>
                </p>
            </div>
        </div>
    </div>
    <div class="dvWelcomeBackground" id="dvWelcomeBackground" style="display: block; opacity: 0.7;"></div>
  
</div>
<script type="text/javascript">
$(document).ready(function(){
   var width = $(window).width();
   setWidth(width);  
   $(window).resize(function() {
        var width = $(window).width();  
        setWidth(width); 
  });
});

function setWidth(width)
{
   if(width <= 768)
   {     
      $('#fn_controll_device').addClass('divHandyDevice');      
   }
   else
   {      
      $('#fn_controll_device').removeClass('divHandyDevice'); 
   }
}
</script>
<style type="text/css">
    /* ----------------------------------------------------
    WELCOME TICKER
----------------------------------------------------- */
.dvWelcomeBackground {
    top: 0px;
    left: 0px;
    opacity: 0;
    width: 100%;
    height: 100%;
    display: none;
    z-index: 9989;
    position: fixed;
    background-color: rgba(0,0,0,0.80);
}
.divWelcomeBox 
{
    left: 50%;
    display: none;
    z-index: 9990;
    max-width: 482px;
    padding: 20px 16px;
    position: absolute;
    margin-left: -241px;
    background-color: #fff;
    border: 4px solid #24890d;
}
.ancModalToolbarX 
{
    top: 10px;
    right: 10px;
    position: absolute;
}
.divWelcomeBox h2{
    color: #24890d;
}
.divWelcomeBox p:last-child
{
    margin-bottom: 0px;
}
.offerImage
{
    float:right;
    margin-left: 12px;
    margin-bottom: 10px;
}
.offerImage img
{
    border: 3px dashed rgba(0,120,10,0.42);
}
.button {
    margin: 0;
    color: #fff;
    cursor: pointer;
    font-size: 12px;
    font-weight: 400;
    padding: 4px 12px;
    line-height: 20px;
    text-align: center;
    position: relative;
    white-space: nowrap;
    background: #57ad68;
    display: inline-block;
    text-decoration: none;
    -webkit-appearance: none;
    text-transform: lowercase;
    border: 2px solid #439f55;
    text-transform: capitalize;
    text-shadow: 0 1px 1px rgba(0,0,0,0.3);
    box-shadow: inset 0 1px 1px rgba(255,255,255,0.2);
    -webkit-box-shadow: inset 0 1px 1px rgba(255,255,255,0.2);
}
a.button
{
    color: white;
    border-radius: 4px;
    text-decoration: none;
}
.btnSmall 
{
    font-size: 12px;
    padding: 6px 12px;
    border: 1px solid;
    line-height: 18px;
    text-transform: capitalize;
}
.btnGreen
{
    color: white;
    text-shadow: none;
    background-color: #35aa47;
    background-image: -moz-linear-gradient(top,#35aa47,#35aa47);
    background-image: -ms-linear-gradient(top,#35aa47,#35aa47);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#35aa47),to(#35aa47));
    background-image: -webkit-linear-gradient(top,#35aa47,#35aa47);
    background-image: -o-linear-gradient(top,#35aa47,#35aa47);
    background-image: linear-gradient(top,#35aa47,#35aa47);
}
.btnGrey
{
    color: white;
    text-shadow: none;
    background-color: #555;
    background-image: -moz-linear-gradient(top,#555,#555);
    background-image: -ms-linear-gradient(top,#555,#555);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#555),to(#555));
    background-image: -webkit-linear-gradient(top,#555,#555);
    background-image: -o-linear-gradient(top,#555,#555);
    background-image: linear-gradient(top,#555,#555);
}
.btnRed 
{
    color: white;
    text-shadow: none;
    background-color: #d84a38;
    background-image: -moz-linear-gradient(top,#dd4b39,#d14836);
    background-image: -ms-linear-gradient(top,#dd4b39,#d14836);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#dd4b39),to(#d14836));
    background-image: -webkit-linear-gradient(top,#dd4b39,#d14836);
    background-image: -o-linear-gradient(top,#dd4b39,#d14836);
    background-image: linear-gradient(top,#dd4b39,#d14836);
}
</style>

