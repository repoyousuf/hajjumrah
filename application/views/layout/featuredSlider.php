<!-- Featured Slider DOM-->
<div class="div_slider-wrapper">
    <div class="slider-inner">
        <div id="da-slider" class="da-slider"> 
            <div class="da-slide">
                <h2><i>Interactive Guide for <br/>Hajj and Umrah Rituals</i></h2>
                <p><i>It will show the path from one ritual to another ritual</i><br/>
                    <i>All Duas or supplication that you are required to perform, based on your day, time and location</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/interactive-guide-for-hajj-and-umrah-rituals'); ?>">View the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>hajjumrah-mobile-hand.png" alt="Interactive Guide for Hajj and Umrah Rituals"></div>
            </div>
            <div class="da-slide">
                <h2><i>Lost and Found Services</i></h2>
                <p><i>A Pilgrim can locate his friends and family members in real-time and also see the path to the friends.</i><br/>
                    <i>Also he/she can go back to his/her favorite added places.</i>
                </p>
                <div class="da-link"><a href="<?php echo site_url('site/features/lost-and-found-services'); ?>">View the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>naxus-slider-featured.png" alt="Lost and Found Services"></div>
            </div>
            <div class="da-slide">
                <h2><i>OUT OF BOUNDARY SERVICES</i></h2>
                <p><i>Do You Know?<br/>knowing the boundary of Haram, Mina, Muzdalifah and Arafat is a part of the Rituals of Hajj?</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>">Check it details</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>laptop-hojjumrah-offer.png" alt="OUT OF BOUNDARY SERVICES"></div>
            </div>
            <div class="da-slide">
                <h2><i>HEALTH SERVICES</i></h2>
                <p><i>Pilgrim will be able to share their health conditions with their companions of Interest along with the location of the pilgrim</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/context-aware-e-health-services'); ?>">View the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>emergency-services-medium.png" alt="HEALTH SERVICES"></div>
            </div>
            <div class="da-slide">
                <h2><i>COMPLAINT SERVICES</i></h2>
                <p><i>Pilgrims can submit a complaint on bus services, tent services and food etc.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/pilgrims-complains-to-respective-authority'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>complaint-services-banner.png" alt="COMPLAINT SERVICES"></div>
            </div>
            <div class="da-slide">
                <h2><i>Hajj Messenger <small>with text, audio</small></i></h2>
                <p><i>A pilgrim may share his memorable moments with his friends and relatives through text and audio message.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/hajj-messenger-with-text-audio'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>hajj-messenger-img.png" alt="Hajj Messenger with text, audio"></div>
            </div>
            <div class="da-slide">
                <h2><i>Offline Maps</i></h2>
                <p><i>A pilgrim can view, search and navigate to “Points of Interests” using Offline Map without requiring Internet.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/offline-maps'); ?>">View Detail</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>offline-maps-img.png" alt="Offline Maps"></div>
            </div>
            <div class="da-slide">
                <h2><i>Places of Interest and Review</i></h2>
                <p><i>Search any “Points of Interest”<br/>Add your Points of Interest to your favorite & Navigate there</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/places-of-interest-and-reviews'); ?>">Know more detail</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>places-of-interest-and-review-img.png" alt="Places of Interest and Review"></div>
            </div>
            <div class="da-slide">
                <h2><i>Free Emergency SMS</i></h2>
                <p><i>Send 3 free Emergency SMS to your relatives or friends anywhere in the world.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/free-emergency-sms-send-emergency-sms-to-your-relatives-or-friends-anywhere-in-the-world'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>free-emergency-SMS-img.png" alt="Free Emergency SMS"></div>
            </div>
            <div class="da-slide">
                <h2><i>Emergency Services</i></h2>
                <p><i>This application provides the pilgrims the facility to call and see the directional map of police station, fire station, ambulance and Hajj Agency.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/emergency-service-and-navigation-to-nearby-emergency-facilities'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>emergency-services-img.png" alt="Emergency Services"></div>
            </div>
            <div class="da-slide">
                <h2><i>Money Exchange for Pilgrims</i></h2>
                <p><i>Find the currency conversion rate and path/direction to your nearest money exchange store.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/money-exchange-find-the-money-conversion-rate-and-nearest-money-from-your-current-location'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>money-exchange-and-currency-converter-img.png" alt="Money Exchange for Pilgrims"></div>
            </div>
            <div class="da-slide">
                <h2><i>Prayer Schedule and Qibla Compass</i></h2>
                <p><i>See the prayer time based on your current location<br/>
                        Show the Qibla direction instantaneously</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/prayer-schedule-see-the-prayer-time-based-on-your-current-location'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>prayer-schedule-and-Qibla-compass-img.png" alt="Prayer Schedule and Qibla Compass"></div>
            </div>
            <div class="da-slide">
                <h2><i>Weather update</i></h2>
                <p><i>Weather alarm while you are in Makkah, Madinah, Jeddah.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/weather-update-plan-your-trip-see-the-weather-of-the-holy-places'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>weather-update-img.png" alt="Weather update"></div>
            </div>
            <div class="da-slide">
                <h2><i>News Services</i></h2>
                <p><i>You may read Hajj and Umrah related news.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/news-services-set-up-to-date-through-our-hajj-and-umrah-related-news'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>news-services-img.png" alt="News Services"></div>
            </div>
            <div class="da-slide">
                <h2><i>Hajj Tweets</i></h2>
                <p><i>Connect with Social Network and get update on Hajj related Post or Post your own tweets.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/hajj-tweets-in-twitter-everyday'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>hajj-tweets-img.png" alt="Hajj Tweets"></div>
            </div>
            <div class="da-slide">
                <h2><i>Translation and-Text to Speech Services</i></h2>
                <p><i>If you have internet connection, Our Translation service will help you to interpret the word or sentence from one language to another.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>translation-and-text-to-speech-services-img.png" alt="Translation and-Text to Speech Services"></div>
            </div>
            <div class="da-slide">
                <h2><i>Traffic Update</i></h2>
                <p><i>Get updated traffic conditions around you in Makkah.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/traffic-update-interactive-crowd-sourced'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>traffic-update-img.png" alt="Traffic Update"></div>
            </div>
            <div class="da-slide">
                <h2><i>Multi-Language Support</i></h2>
                <p><i>App provides user interface in different languages.</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/multi-language-support-for-pilgrims'); ?>">Check out the service</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>multi-language-support-img.png" alt="Multi-Language Support"></div>
            </div>
            <div class="da-slide">
                <h2><i>Easy Installation and Registration via Free SMS Authentication</i></h2>
                <p><i>Just feed your basic details and we take care of the rest!</i></p>
                <div class="da-link"><a href="<?php echo site_url('site/features/easy-installation-and-registration-via-free-sms-authentication'); ?>">View Detail</a></div>
                <div class="da-img"><img class="img-responsive" src="<?php echo IMG_URL; ?>registration-via-free-SMS-img.png" alt="Easy Installation and Registration via Free SMS Authentication"></div>
            </div>
            <div class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>		
            </div>
        </div>
    </div>
</div>  
<div class="featuresNav">
    <div class="layout-main">
        <div class="container">
            <div class="row" style="display:none;">
                <div class="twelve columns">
                    <div id="myDropdown">
                        <ul>
                            <li class="dropdown selected">
                                <a href="javascript:void(0);"><i class="fa fa-home"></i> Features Application Menu</a>
                                <ul class="applicationMenu">                        
                                    <li>
                                        <a href="<?php echo site_url('site/features/interactive-guide-for-hajj-and-umrah-rituals'); ?>">Interactive Guide for Hajj and Umrah Rituals</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/lost-and-found-services'); ?>">Lost and Found Services</a>                                    
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>">Out of Boundary Services</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/places-of-interest-and-reviews'); ?>">Places of Interest and Reviews</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/money-exchange-find-the-money-conversion-rate-and-nearest-money-from-your-current-location'); ?>">Money Exchange</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/prayer-schedule-see-the-prayer-time-based-on-your-current-location'); ?>">Prayer schedule</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/weather-update-plan-your-trip-see-the-weather-of-the-holy-places'); ?>">Weather update</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/news-services-set-up-to-date-through-our-hajj-and-umrah-related-news'); ?>">News Services</a> 
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/hajj-tweets-in-twitter-everyday'); ?>">Hajj Tweets</a> 
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/emergency-service-and-navigation-to-nearby-emergency-facilities'); ?>">Emergency Services</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>">Translation and Text to Speech Services</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/context-aware-e-health-services'); ?>">Health Services</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/pilgrims-complains-to-respective-authority'); ?>">Complaint Services</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/hajj-messenger-with-text-audio'); ?>">Hajj Messenger with text, audio</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/free-emergency-sms-send-emergency-sms-to-your-relatives-or-friends-anywhere-in-the-world'); ?>">Free Emergency SMS </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/traffic-update-interactive-crowd-sourced'); ?>">Traffic Update </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/multi-language-support-for-pilgrims'); ?>">Multi-Language Support </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('site/features/easy-installation-and-registration-via-free-sms-authentication'); ?>">Easy Installation and Registration via Free SMS Authentication</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="follow-us-ul">
                        <ul class="unstyled inline">
                            <li><strong>Follow Us :</strong></li>
                            <li><a title="Facebook" href="https://www.facebook.com/pages/Hajj-Umrah-Mobile-App/1486164448292981"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a title="Google Plus" href="https://plus.google.com/u/2/b/110508884895775157167/110508884895775157167/about"><i class="fa fa-google-plus-square"></i></a></a></li>
                            <li><a title="Twitter" href="https://twitter.com/advMediaLabBD"><i class="fa fa-twitter-square"></i></a></a></li>
                            <li><a title="Youtube" href="https://www.youtube.com/user/theadvancedmedialab"><i class="fa fa-youtube-square"></i></a></a></li>
                            <li><a title="Linkedin" href="#"><i class="fa fa-linkedin-square"></i></a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row" style="display:none;">
                <div class="twelve columns">
                    <ul class="unstyled inline">                        
                        <?php
                        $slug = $this->uri->segment(3);
                        ?>
                        <li class="<?php
                        if (isset($slug) && $slug == 'personalized-and-spatio-temporal-hajj-and-umrah-ritual-services') {
                            echo 'selected';
                        }
                        ?> ">
                            <a href="<?php echo site_url('site/features/personalized-and-spatio-temporal-hajj-and-umrah-ritual-services'); ?>">Hajj and Umrah Services</a>
                        </li>
                        <li class="<?php
                        if (isset($slug) && $slug == 'out-of-boundary-services') {
                            echo 'selected';
                        }
                        ?> ">
                            <a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>">Boundary Services</a>
                        </li>
                        <li class="<?php
                            if (isset($slug) && $slug == 'location-and-time-aware-services') {
                                echo 'selected';
                            }
                        ?> ">
                            <a href="<?php echo site_url('site/features/location-and-time-aware-services'); ?>">Location and Time </a> 
                        </li>
                        <li class="<?php
                            if (isset($slug) && $slug == 'translation-and-text-to-speech-services') {
                                echo 'selected';
                            }
                        ?> ">
                            <a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>">Translation</a>
                        </li>
                        <li class="<?php
                        if (isset($slug) && $slug == 'emergency-services') {
                            echo 'selected';
                        }
                        ?> ">
                            <a href="<?php echo site_url('site/features/emergency-services'); ?>">Emergency Services</a>
                        </li>
                        <li class="<?php
                        if (isset($slug) && $slug == 'twitter-services') {
                            echo 'selected';
                        }
                        ?> ">
                            <a href="<?php echo site_url('site/features/twitter-services'); ?>">Twitter Services</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="leadtext">
    <div class="layout-main">
        <div class="container">
            <div class="row">
                <h1 title="Hajj Umrah Management Systems Features">Hajj Umrah Management Systems Features</h1>
                <p class="leadItalic"><strong>Are you looking for a smart hajj companion? </strong><br/> Our app will provide all the necessary steps that are required to perform Hajj, based on day, time and location. Our app will help you to track fard, wajib and sunnah of Hajj, based on your current location. It will help you to make your hajj perfect. <br/><strong>Some key features are following:</strong></p>
                <span><i class="fa fa-angle-double-down"></i></span>
            </div>
        </div>
    </div>
</div>
