<aside>
    <div>
        <section id="signUp" class="sign-up-form">
            <h1>Subscribe Here</h1>
            <div>
                <p class="intro"> To Get Instant Update Of Hajj & Umrah Services</p>
                <form method="post" action="<?php echo site_url('site/signup'); ?>" id="signupform" name="signupform">
                    <div>
                        <p>* Required fields</p>
                    </div>
                    <div> 
                        <label>Name<span class="required">*</span></label>
                        <input name="name" id="name" autocomplete="off" value="" type="text" class="required"/> 
                    </div>                                 
                    <div> 
                        <?php
                        /* $result = '';
                          $location = @file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);

                          $data     =    json_decode($location);
                          if(!empty ($data))
                          {
                          $result  = get_country_dial_code(trim($data->country_code));
                          } */
                        $data = get_country_dial_code();
                        ?>
                        <label>Mobile<span class="required">*</span></label>
                        <input name="dial_code" id="dial_code" value="" type="hidden" /> 
                        <div class="custom-select"><i>Country Code </i>
                            <select name="country_code" id="country_code" style="width: 140px;">
                                 <option value="">--Select--</option>
                                <?php foreach ($data AS $arr) { ?>
                                    <option value="<?php echo ucwords($arr['dial_code']); ?>"><?php echo ucwords(strtolower($arr['country_name'])); ?></option>
                                <?php } ?>
                            </select>
                            <span class="dial_code"></span>
                            <br/><i>Your Number </i> <br/><input name="mobile" autocomplete="off" style="max-width: 384px;" id="mobile" value="" type="text" class="required number"/></div>
                    </div>
                    <div> 
                        <label>Email <span class="required">*</span></label> 
                        <input name="email" id="email" value="" type="text" autocomplete="off"  class="fn_email_exists required email"/>
                    </div>                 
                    <div> 
                        <input value="Submit" class="button" type="submit" /> 
                        <input value="Reset" class="button" type="reset" /> 
                    </div>
                </form>                
            </div>
        </section>
        <?php /*?><section class="follow-us-section">
            <h2>Follow Us</h2>
            <ul class="link-list social">
                <li class="facebook"><a href="https://www.facebook.com/pages/Hajj-Umrah-Mobile-App/1486164448292981">Facebook</a></li>
                <li class="googleplus"><a href="https://plus.google.com/u/2/b/110508884895775157167/110508884895775157167/about">Google+</a></li>
                <li class="twitter"><a href="https://twitter.com/advMediaLabBD">Twitter</a></li>
                <li class="youtube"><a href="https://www.youtube.com/user/theadvancedmedialab">YouTube</a></li>
                <li class="linkedin"><a href="#">Linkedin</a></li>
            </ul>
        </section><?php */?>
     
    </div>
</aside>  
 <section class="utility-follow-sec">
            <h2>Follow Us</h2>
            <ul class="link-list social">
                <li class="facebook"><a target="_blank" href="https://www.facebook.com/pages/Hajj-Umrah-Mobile-App/1486164448292981">Facebook</a></li>
                <li class="googleplus"><a target="_blank" href="https://plus.google.com/u/2/101819255724771261240/">Google+</a></li>
                <li class="twitter"><a target="_blank" href="https://twitter.com/advMediaLabBD">Twitter</a></li>
                <li class="youtube"><a target="_blank" href="https://www.youtube.com/user/theadvancedmedialab">YouTube</a></li>
                <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/pub/advanced-media-lab/a3/570/21a">Linkedin</a></li>
            </ul>
        </section>

    
