<?php
$subject = $this->uri->segment(2);
$slug = $this->uri->segment(3);
?>

 <?php if( $subject != '' || $slug != ''){ ?>
<div class="container">
    <div class="row">
        <ul class="page-breadcrumb breadcrumb">
            <li><small>You are now :</small></li>
            <li><i class="fa fa-home"></i><a href="<?php echo site_url(); ?>">Home</a></li>
            <?php if( $subject != '' && $slug != ''){ ?>
            <li><i class="fa fa-angle-right"></i><a href="<?php echo site_url('site/whatweoffer/'); ?>">What We Offer </a><i class="fa fa-angle-right"></i></li>
            <li><span><?php echo $title_for_layout; ?></span></li>
            <?php } ?>            
             <?php if( $subject != '' && $slug == ''){ ?>
                <li><i class="fa fa-angle-right"></i><span><?php echo $title_for_layout; ?></span></li>
             <?php } ?>
        </ul>
    </div>
</div>
<?php } ?>