<?php
    $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
    $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
    $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
    $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
    $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");
   
?>
<!-- Download this app bar-->
<?php if (($iPod || $iPhone || $iPad || $Android) && $activeTab == 'download') { ?>
<div class="download-this">
        <span class="tiny-logo"><img alt="Hajjumrah Mobile App" src="<?php echo IMG_URL; ?>hajjumrah-download-logo.png"></span>
        <div>
            <h5 title="Download this app"><small><?php if ($iPod || $iPhone || $iPad) { ?> 
                    <a href="https://itunes.apple.com/us/app/perform-hajj-umrah/id917265874?ls=1&mt=8" title="Downloading Our Hajjumrah mobile application on iTunes is available now" target="_blank">Downloading <strong>Hajjumrah Mobile App </strong> for IOS users are coming soon. <button>Install</button></a>
                    <?php } else if ($Android) { ?>
                        <a href="https://play.google.com/store/apps/details?id=com.hajjandumrah&hl=en" target="_blank">Downloading <strong>Hajjumrah Mobile App </strong> for Android users is available now <button>Install</button></a>
                    <?php } else { ?>
                        <a href="http://advancedmedialab.com/hajjumrah/"><strong>Visit Our Hajjumrah Website</strong></a>
                    <?php } ?></small></h5>
            <label><strong>Media Lab.</strong><i>Umm Al Qura, University, KSA</i></label>        
        </div>
        <button type="button" class="button-dismiss" title="Clos this">×</button>
    </div>
<?php } ?>
<div id="header-wrap" style="top: auto;">
    <div class="layout-main">
        <div class="container">
            <div class="row">
                <div class="twelve columns">
                    <header>
                        <hgroup>
                            <h1><a href="http://www.advancedmedialab.com">Media Lab</a></h1>
                            <h3>Umm Al Qura University</h3>
                        </hgroup>
                        <nav>
                            <div class="hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <ul>
                                <li class="<?php if(isset ($activeTab) && $activeTab == 'home'){ echo 'selected'; } ?> "><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                                <li class="dropdown features-sub <?php if(isset ($activeTab) && $activeTab == 'whatweoffer'){ echo 'selected'; } ?> "><a href="javascript:void(0);"><i class="fa fa-bell"></i> What We Offer<sup>Features</sup></a>
                                        <ul>                        
                                            <li>
                                                <a href="<?php echo site_url('site/features/interactive-guide-for-hajj-and-umrah-rituals'); ?>">Interactive Guide for Hajj and Umrah Rituals</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/lost-and-found-services'); ?>">Lost and Found Services</a>                                    
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>">Out of Boundary Services</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/context-aware-e-health-services'); ?>">Health Services</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/pilgrims-complains-to-respective-authority'); ?>">Complaint Services</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/hajj-messenger-with-text-audio'); ?>">Hajj Messenger</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/offline-maps'); ?>">Offline Maps</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/places-of-interest-and-reviews'); ?>">Places of Interest and Reviews</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/free-emergency-sms-send-emergency-sms-to-your-relatives-or-friends-anywhere-in-the-world'); ?>">Free Emergency SMS </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/emergency-service-and-navigation-to-nearby-emergency-facilities'); ?>">Emergency Services</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/money-exchange-find-the-money-conversion-rate-and-nearest-money-from-your-current-location'); ?>">Money Exchange</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/prayer-schedule-see-the-prayer-time-based-on-your-current-location'); ?>">Prayer schedule</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/weather-update-plan-your-trip-see-the-weather-of-the-holy-places'); ?>">Weather update</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/news-services-set-up-to-date-through-our-hajj-and-umrah-related-news'); ?>">News Services</a> 
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/hajj-tweets-in-twitter-everyday'); ?>">Hajj Tweets</a> 
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>">Translation and Text to Speech Services</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/traffic-update-interactive-crowd-sourced'); ?>">Traffic Update </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/multi-language-support-for-pilgrims'); ?>">Multi-Language Support </a>
                                            </li>
                                            <li>
                                                <a href="<?php echo site_url('site/features/easy-installation-and-registration-via-free-sms-authentication'); ?>">Easy Installation and Registration via Free SMS Authentication</a>
                                            </li>
                                        </ul>
                                </li>
                                <li class="<?php if(isset ($activeTab) && $activeTab == 'download'){ echo 'selected'; } ?> "> <a style="color: #e44d39;" href="<?php echo site_url('site/download'); ?>">Download</a></li>            
                                <li class="<?php if(isset ($activeTab) && $activeTab == 'tutorials'){ echo 'selected'; } ?> "><a  href="<?php echo site_url('site/tutorials'); ?>">FAQ / Video Tutorial</a>
                                    <ul style="display: none;">                     
                                        <li><a href="<?php echo site_url('site/videos'); ?>">Videos</a></li>
                                        <li><a href="<?php echo site_url('site/manuals'); ?>">Manuals</a></li>
                                        <li><a href="<?php echo site_url('site/faq'); ?>">FAQ</a></li>
                                    </ul>
                                </li>
                                <li class="<?php if(isset ($activeTab) && $activeTab == 'contactus'){ echo 'selected'; } ?> "><a href="<?php echo site_url('site/contactus'); ?>">Contact Us</a></li>
                            </ul>
                        </nav>
                    </header>
                </div>
            </div>
        </div>
    </div>
</div>