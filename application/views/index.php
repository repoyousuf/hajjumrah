<section id="main">
  <div class="container">
    <div class="row">
      <div class="twelve columns"> <?php echo $this->load->view('layout/message'); ?> </div>
    </div>
    <div class="row">
      <div class="nine columns">
          <div style="display:none;">
              <h1 title="Hajj Umrah Management Systems Features">Hajj Umrah Management Systems Features</h1>
              <p class="leadItalic"><strong>Are you looking for a smart hajj companion? </strong><br/>
                  Our app will provide all the necessary steps that are required to perform Hajj, based on day, time and location. Our app will help you to track fard, wajib and sunnah of Hajj, based on your current location. It will help you to make your hajj perfect. Some key features are following:</p>
          </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/interactive-guide-for-hajj-and-umrah-rituals'); ?>"><img alt="Interactive Guide for Hajj and Umrah Rituals" src="<?php echo IMG_URL; ?>features/interactive-guide-for-hajj-and-umrah-rituals-thumb.jpg"></a>
              <h3> Interactive Guide for Hajj and Umrah Rituals </h3>
              <p> Performing Hajj and Umrah Rituals have never been so user friendly. </p>
              <p> <a href="<?php echo site_url('site/features/interactive-guide-for-hajj-and-umrah-rituals'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/lost-and-found-services'); ?>"><img alt="Lost and Found Services" src="<?php echo IMG_URL; ?>features/lost-and-found-services-thumb.jpg"></a>
              <h3> Lost and Found Services </h3>
              <p> case you are lost, We will guide you to reach to your friends </p>
              <p> <a href="<?php echo site_url('site/features/lost-and-found-services'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>"><img alt="Out of Boundary Services" src="<?php echo IMG_URL; ?>features/Out-of-boundry-services-thumb.jpg"></a>
              <h3> Out of Boundary Services </h3>
              <p> Staying inside the Hudud is required to perform the Hajj </p>
              <p> <a href="<?php echo site_url('site/features/out-of-boundary-services'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/context-aware-e-health-services'); ?>"><img alt="Health Services" src="<?php echo IMG_URL; ?>features/health-services-thumb.jpg"></a>
              <h3> Health Services </h3>
              <p> Not feeling well? Submit the health form. We will forward </p>
              <p> <a href="<?php echo site_url('site/features/context-aware-e-health-services'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/pilgrims-complains-to-respective-authority'); ?>"><img alt="Complaint Services" src="<?php echo IMG_URL; ?>features/complaint-services-thumb.jpg"></a>
              <h3> Complaint Services </h3>
              <p> Not Happy with the agent, report the problem to your agent. </p>
              <p> <a href="<?php echo site_url('site/features/pilgrims-complains-to-respective-authority'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/hajj-messenger-with-text-audio'); ?>"><img alt="Hajj Messenger with text, audio" src="<?php echo IMG_URL; ?>features/Hajj-messenger-with-text-list.png"></a>
              <h3> Hajj Messenger with text, audio </h3>
              <p> Chatting and sharing images with your other pilgrims friend </p>
              <p> <a href="<?php echo site_url('site/features/hajj-messenger-with-text-audio'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/offline-maps'); ?>"> <img alt="Offline Maps" src="<?php echo IMG_URL; ?>features/offline-maps-thumb.jpg"></a>
              <h3> Offline Maps </h3>
              <p> A pilgrim can view, search and navigate to “Points of Interests” using Offline Map without requiring Internet. </p>
              <p> <a href="<?php echo site_url('site/features/offline-maps'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/places-of-interest-and-reviews'); ?>"><img alt="Places of Interest and Reviews" src="<?php echo IMG_URL; ?>features/places-of-interest-and-reviews-thumb.jpg"></a>
              <h3> Places of Interest and Reviews </h3>
              <p> Searching Place of Interest, finding the reviews about </p>
              <p> <a href="<?php echo site_url('site/features/places-of-interest-and-reviews'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/free-emergency-sms-send-emergency-sms-to-your-relatives-or-friends-anywhere-in-the-world'); ?>"><img alt="Free Emergency SMS" src="<?php echo IMG_URL; ?>features/free-emergency-SMS-thumb.jpg"></a>
              <h3> Free Emergency SMS </h3>
              <p> Send Emergency SMS to your relatives or friends anywhere in the world. </p>
              <p> <a href="<?php echo site_url('site/features/free-emergency-sms-send-emergency-sms-to-your-relatives-or-friends-anywhere-in-the-world'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/emergency-service-and-navigation-to-nearby-emergency-facilities'); ?>"><img alt="Emergency Services" src="<?php echo IMG_URL; ?>features/emergency-services-thumb.jpg"></a>
              <h3> Emergency Services </h3>
              <p> Emergency service and navigation to nearby Emergency facilities </p>
              <p> <a href="<?php echo site_url('site/features/emergency-service-and-navigation-to-nearby-emergency-facilities'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/money-exchange-find-the-money-conversion-rate-and-nearest-money-from-your-current-location'); ?>"><img alt="Money Exchange" src="<?php echo IMG_URL; ?>features/currency-converter-img-list.png"></a>
              <h3> Money Exchange and Currency converter </h3>
              <p> Find the Money conversion rate and nearest money </p>
              <p> <a href="<?php echo site_url('site/features/money-exchange-find-the-money-conversion-rate-and-nearest-money-from-your-current-location'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/prayer-schedule-see-the-prayer-time-based-on-your-current-location'); ?>"><img alt="Prayer schedule" src="<?php echo IMG_URL; ?>features/prayer-schedule-thumb.jpg"></a>
              <h3> Prayer schedule And Qibla Compass </h3>
              <p> See the prayer time based on your current location </p>
              <p> <a href="<?php echo site_url('site/features/prayer-schedule-see-the-prayer-time-based-on-your-current-location'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/weather-update-plan-your-trip-see-the-weather-of-the-holy-places'); ?>"><img alt="Weather update" src="<?php echo IMG_URL; ?>features/weather-update-thumb.jpg"></a>
              <h3> Weather update </h3>
              <p> Plan your trip!!! See the Weather of the holy places. </p>
              <p> <a href="<?php echo site_url('site/features/weather-update-plan-your-trip-see-the-weather-of-the-holy-places'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/news-services-set-up-to-date-through-our-hajj-and-umrah-related-news'); ?>"><img alt="News Services Set Up to date Through Our Hajj And Umrah Related News Services" src="<?php echo IMG_URL; ?>features/news-services-thumb.jpg"></a>
              <h3> News Services </h3>
              <p> Get up-to-date through our Hajj and Umrah related news </p>
              <p> <a href="<?php echo site_url('site/features/news-services-set-up-to-date-through-our-hajj-and-umrah-related-news'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/hajj-tweets-in-twitter-everyday'); ?>"><img alt="Hajj Tweets" src="<?php echo IMG_URL; ?>features/hajj-tweets-thumb.jpg"></a>
              <h3> Hajj Tweets </h3>
              <p> Connecting with Social and Network and getting update on </p>
              <p> <a href="<?php echo site_url('site/features/hajj-tweets-in-twitter-everyday'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>"><img alt="Translation and Text to Speech Services" src="<?php echo IMG_URL; ?>features/translation-and-text-to-speech-services-list.png"></a>
              <h3> Translation and Text to Speech Services </h3>
              <p> Facing problem in translation, we will help you out </p>
              <p> <a href="<?php echo site_url('site/features/translation-and-text-to-speech-services'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/traffic-update-interactive-crowd-sourced'); ?>"><img alt="Traffic Update" src="<?php echo IMG_URL; ?>features/traffic-update-thumb.jpg"></a>
              <h3> Traffic Update </h3>
              <p> Interactive Crowd sourced traffic update </p>
              <p> <a href="<?php echo site_url('site/features/traffic-update-interactive-crowd-sourced'); ?>">More…</a> </p>
            </div>
          </div>
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/multi-language-support-for-pilgrims'); ?>"><img alt="Multi-Language Support" src="<?php echo IMG_URL; ?>features/multi-language-support-list.png"></a>
              <h3> Multi-Language Support </h3>
              <p> App provides user interface in different languages like English </p>
              <p> <a href="<?php echo site_url('site/features/multi-language-support-for-pilgrims'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
        <div class="row featured-items">
          <div class="four column">
            <div class="thumbnail-style"> <a href="<?php echo site_url('site/features/easy-installation-and-registration-via-free-sms-authentication'); ?>"><img alt="Easy Installation and Registration via Free SMS Authentication" src="<?php echo IMG_URL; ?>features/sample-featured-img-01.jpg"></a>
              <h3> Easy Installation and Registration via Free SMS Authentication </h3>
              <p> Just feed your basic details and we take care of the rest. </p>
              <p> <a href="<?php echo site_url('site/features/easy-installation-and-registration-via-free-sms-authentication'); ?>">More…</a> </p>
            </div>
          </div>
        </div>
      </div>
      <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
    </div>
  </div>
</section>
  <?php echo $this->load->view('layout/welcomethis'); ?>
