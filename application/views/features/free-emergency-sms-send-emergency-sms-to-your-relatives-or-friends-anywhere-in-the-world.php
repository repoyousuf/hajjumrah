<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="row">
        <div class="nine columns">
            <h1>Free Emergency SMS</h1>
            <div class="detail_description">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="custom-frame">              
                                <div id="divFreeEmergencySMS" class="carousel slide div-featured-slide">
                                    <div class="carousel-inner">
                                        <div class="item active"> <img alt="Free Emergency SMS" src="<?php echo IMG_URL; ?>features/slider-banner/free-emergency-SMS-01-img.png" alt="Free Emergency SMS">
                                            <div class="carousel-caption">
                                                <p>Free Emergency SMS</p>
                                            </div>
                                        </div>
                                        <div class="item"><img alt="Free Emergency SMS 02" src="<?php echo IMG_URL; ?>features/slider-banner/free-emergency-SMS-02-img.png" alt="Free Emergency SMS">
                                            <div class="carousel-caption">
                                                <p>Free Emergency SMS</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-arrow"> 
                                        <a class="left carousel-control" href="#divFreeEmergencySMS" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                                        <a class="right carousel-control" href="#divFreeEmergencySMS" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                                    </div>
                                </div>
                            </td>

                            <td><ul><li>Send 3 free Emergency SMS to your relatives or friends anywhere in the world.</li></ul></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
    </div>
</div>
