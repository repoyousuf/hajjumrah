<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
        <div class="nine columns">
      <h1>Health Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Health Services" src="<?php echo IMG_URL; ?>features/health-services-img.jpg"/></td>
              <td><ul><li>Pilgrim will be able to share their health conditions with their companions of Interest along with the location of the pilgrim.</li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
        <div class="three columns">
                <?php echo $this->load->view('layout/aside'); ?>
        </div>
    </div>
</div>
