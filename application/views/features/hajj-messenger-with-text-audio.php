<?php
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
?>

<div class="container">
    <div class="row">
        <div class="nine columns">
            <h1>Hajj Messenger with text, audio</h1>
            <div class="detail_description">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><ul><li>A pilgrim may share his memorable moments with his friends and relatives through text and audio message.</li></ul></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="videoYoutubeRegister" class="divVideoContainer">
                                    <iframe src="https://www.youtube.com/embed/EiGdfak67vo" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="three columns">
            <?php echo $this->load->view('layout/aside'); ?>
        </div>
    </div>
</div>
