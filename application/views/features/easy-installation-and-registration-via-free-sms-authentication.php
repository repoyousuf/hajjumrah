<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
        <div class="nine columns">
            <h1>Easy Installation and Registration via Free SMS Authentication</h1>
            <div class="detail_description">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><ul><li>Just feed your basic details and we take care of the rest!</li></ul></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="videoYoutubeRegister" class="divVideoContainer">
                                    <iframe src="https://www.youtube.com/embed/RKlpQqVTilM" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
    </div>
</div>
