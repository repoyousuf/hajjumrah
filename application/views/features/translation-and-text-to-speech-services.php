<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Translation and-Text to Speech Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Translation and-Text to Speech Services" src="<?php echo IMG_URL; ?>features/translation-and-text-img.jpg"/></td>
              <td><ul>
                  <li>Translation Service: Translation service is used to interpret the word or sentence from one language to another. (requiresInternet)</li>
                  <li>Text to speech service: The translation service is augmented with a text to speech service in which the translated text is also spelled out to facilitate a pilgrim to understand a conversation. </li>
                </ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
