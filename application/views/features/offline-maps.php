<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Offline Maps</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
           <tr>
              <td class="custom-frame">
                <img alt="Offline Maps" src="<?php echo IMG_URL; ?>features/offline-maps-img.jpg"/></td>
              <td><ul><li>A pilgrim can view, search and navigate to “Points of Interests” using Offline Map without requiring Internet.</li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns">
                <?php echo $this->load->view('layout/aside'); ?>
    </div>
  </div>
</div>
