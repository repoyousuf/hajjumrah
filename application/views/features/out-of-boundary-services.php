<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Out of Boundary Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame">
                  <div id="divOutOfBoundary" class="carousel slide div-featured-slide">
                    <div class="carousel-inner">
                        <div class="item active"> <img src="<?php echo IMG_URL; ?>features/slider-banner/out-of-boundary-services-01-img.png" alt="Out of Boundary Services">
                            <div class="carousel-caption">
                                <p>Out of Boundary Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/out-of-boundary-services-02-img.png" alt="Out of Boundary Services 2">
                            <div class="carousel-caption">
                                <p>Out of Boundary Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/out-of-boundary-services-03-img.png" alt="Out of Boundary Services 3">
                            <div class="carousel-caption">
                                <p>Out of Boundary Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/out-of-boundary-services-04-img.png" alt=" Out of Boundary Services 4">
                            <div class="carousel-caption">
                                <p>Out of Boundary Services</p>
                            </div>
                        </div>
                      
                    </div>
                    <div class="carousel-arrow"> 
                        <a class="left carousel-control" href="#divOutOfBoundary" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                        <a class="right carousel-control" href="#divOutOfBoundary" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                    </div>
                </div>
              </td>
              <td><ul><li> Since knowing the boundary of Haram, Mina, Muzdalifah and Arafat is a part of the Rituals of Hajj, theapp will show the pilgrims ifthey are inside or outside the Haram, Mina, Muzdalifah and Arafat boundary. </li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
