<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Lost and Found Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame">
                  <div id="divLostFoundServices" class="carousel slide div-featured-slide">
                    <div class="carousel-inner">
                        <div class="item active"> <img src="<?php echo IMG_URL; ?>features/slider-banner/lost-and-found-services-01-img.png" alt="Lost and Found Services">
                            <div class="carousel-caption">
                                <p>Lost and Found Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/lost-and-found-services-02-img.png" alt="Lost and Found Services 2">
                            <div class="carousel-caption">
                                <p>Lost and Found Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/lost-and-found-services-03-img.png" alt="Lost and Found Services 3">
                            <div class="carousel-caption">
                                <p>Lost and Found Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/lost-and-found-services-04-img.png" alt="Lost and Found Services 4">
                            <div class="carousel-caption">
                                <p>Lost and Found Services</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/lost-and-found-services-05-img.png" alt="Lost and Found Services 5">
                            <div class="carousel-caption">
                                <p>Lost and Found Services</p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-arrow"> 
                        <a class="left carousel-control" href="#divLostFoundServices" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                        <a class="right carousel-control" href="#divLostFoundServices" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                    </div>
                </div>
              </td>
              <td>
              <ul>
                  <li>Friends: A Pilgrim can locate his friends and family members in real-time and also see the path to the friends.</li>
                  <li>Favorite Places: A Pilgrim can go back to his/her favorite added places such as Arafat and Mina tent, Hotels in Makkah and Restaurants etc.</li>
                </ul>
             </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
