<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Location based Places of interest</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Location based Places of interest" src="<?php echo IMG_URL; ?>mobile-app-img/place-of-interest.png"/></td>
              <td><p>A pilgrim will be able to
                  browse the points of interests based on his country, preferences and location.
                  give the review of each point of interest
                  see the reviews of each point of interest to learn more about each point of interest
                  see the nearest point of interest among a pool of similar item </p></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns">
                <?php echo $this->load->view('layout/aside'); ?>
    </div>
  </div>
</div>
