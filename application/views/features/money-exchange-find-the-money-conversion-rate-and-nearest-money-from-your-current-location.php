<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Money Exchange</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img src="<?php echo IMG_URL; ?>features/money-exchange-img.jpg" alt="Money Exchange"/></td>
              <td><ul><li>Find the currency conversion rate and path/direction to your nearest money exchange store.</li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
