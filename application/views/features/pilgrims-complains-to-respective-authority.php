<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Complaint Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Complaint Services" src="<?php echo IMG_URL; ?>features/complaint-services-img.jpg"/></td>
              <td><ul><li>Pilgrims can submit a complaint on bus services, tent services and food services etc, to the appropriate authority with the location of complaint.</li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
