<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
    <div class="nine columns">
      <h1>Location based Places of interest</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Location based Places of interest" src="<?php echo IMG_URL; ?>mobile-app-img/place-of-interest.png"/></td>
              <td><p>Aliquam feugiat pretium ligula ac convallis. Proin tempor odio id porta tincidunt. Suspendisse potenti. Curabitur non ante eget sapien condimentum rutrum et sed arcu. Cras urna nibh, pretium at tellus nec, sollicitudin placerat ligula. Aenean ligula dui, laoreet non convallis eget, aliquam eu risus. Ut interdum venenatis tellus nec auctor. Donec eu augue at est vulputate ultrices. Nulla viverra sem non ipsum fringilla iaculis. Donec quis quam porttitor, luctus lectus id, vulputate odio. Sed rhoncus maximus sapien nec fringilla. Donec vel mollis mi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed rutrum nulla a metus pharetra, vitae aliquam lorem vulputate. Duis tempus nisl eget tellus tempus malesuada. Vivamus sit amet sollicitudin neque, sit amet faucibus leo. </p></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
        <div class="three columns">
                <?php echo $this->load->view('layout/aside'); ?>
        </div>
    </div>
</div>
