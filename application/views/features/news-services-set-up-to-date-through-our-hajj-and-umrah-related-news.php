<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>News Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img src="<?php echo IMG_URL; ?>features/news-services-img.jpg" alt="News Services"/></td>
              <td><ul><li>You may read Hajj and Umrah related news.</li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
