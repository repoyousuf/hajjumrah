<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Emergency Services</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Emergency Services" src="<?php echo IMG_URL; ?>features/emergency-services-img.jpg"/></td>
              <td><ul><li> Most of the pilgrims who come to Saudi Arabia to perform Hajj or Umrah are not aware about Emergency phone numbers and its location. Emergency service provides them the facility to call and see the directional map of police station, fire station, ambulance and Hajj Agency. The application provides all this information </li></ul></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
