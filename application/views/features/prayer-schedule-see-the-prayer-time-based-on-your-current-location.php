<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="row">
        <div class="nine columns">
            <h1>Prayer Schedule and Qibla Compass </h1>
            <div class="detail_description">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="custom-frame">
                                <div id="divFreeEmergencySMS" class="carousel slide div-featured-slide">
                                    <div class="carousel-inner">
                                        <div class="item active"> <img src="<?php echo IMG_URL; ?>features/slider-banner/prayer-schedule-01-img.png" alt="Prayer Schedule and Qibla Compass">
                                            <div class="carousel-caption">
                                                <p>Prayer Schedule and Qibla Compass</p>
                                            </div>
                                        </div>
                                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/prayer-schedule-02-img.png" alt="Prayer Schedule and Qibla Compass 2">
                                            <div class="carousel-caption">
                                                <p>Prayer Schedule and Qibla Compass</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-arrow"> 
                                        <a class="left carousel-control" href="#divFreeEmergencySMS" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                                        <a class="right carousel-control" href="#divFreeEmergencySMS" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                                    </div>
                                </div>
                            </td>
                            <td>
                                <ul>
                                    <li>See the prayer time based on your current location</li>
                                    <li>See the time remaining for the next prayer</li>
                                    <li>Show the Qibla direction instantaneously without any calibration or hassle of setting your city</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
    </div>
</div>
