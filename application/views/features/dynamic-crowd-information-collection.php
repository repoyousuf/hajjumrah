<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="row">
        <div class="nine columns">
      <h1>Dynamic Crowd Information Collection</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame"><img alt="Dynamic Crowd Information Collection" src="<?php echo IMG_URL; ?>mobile-app-img/crowd-information-collection.png"/></td>
              <td><p>This Service is used to collect the road dynamic conditions through  crowdsourcing. These dynamic 
road conditions helps user to recommend best path that avoid accident, road blocked, congestion, 
road closed  </p></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
        <div class="three columns">
                <?php echo $this->load->view('layout/aside'); ?>
        </div>
    </div>
</div>
