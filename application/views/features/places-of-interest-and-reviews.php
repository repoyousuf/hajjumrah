<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Places of Interest and Reviews</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame">
                  <div id="divPlacesofInterest" class="carousel slide div-featured-slide">
                    <div class="carousel-inner">
                        <div class="item active"> <img src="<?php echo IMG_URL; ?>features/slider-banner/places-of-interest-and-reviews-01-img.png" alt="Places of Interest and Reviews">
                            <div class="carousel-caption">
                                <p>Places of Interest and Reviews </p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/places-of-interest-and-reviews-02-img.png" alt="Places of Interest and Reviews 2">
                            <div class="carousel-caption">
                                <p>Places of Interest and Reviews </p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/places-of-interest-and-reviews-03-img.png" alt="Places of Interest and Reviews 3">
                            <div class="carousel-caption">
                                <p>Places of Interest and Reviews </p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-arrow"> 
                        <a class="left carousel-control" href="#divPlacesofInterest" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                        <a class="right carousel-control" href="#divPlacesofInterest" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                    </div>
                </div>
              </td>
              <td><ul>
                  <li>We have added Points of Interest like restaurants, hotels, Arafat toilets, Mina toilets etc.</li>
                  <li>Search any “Points of Interest”</li>
                  <li>Add new “Points of Interest” and add it as your favorite.</li>
                  <li>Write review on “Points of Interests” and Favorites. Also you can see the reviews given by other users.</li>
                  <li>Navigate to any “Point of Interest” or favorite.</li>
                </ul>
                </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
