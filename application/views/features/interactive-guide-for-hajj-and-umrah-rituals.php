<div class="container">
  <div class="row">
    <div class="nine columns">
      <h1>Interactive Guide for Hajj and Umrah Rituals</h1>
      <div class="detail_description">
        <table class="table">
          <tbody>
            <tr>
              <td class="custom-frame">
                  <div id="divRituals" class="carousel slide" style="width: 262px;">
                    <div class="carousel-inner">
                        <div class="item active"> <img src="<?php echo IMG_URL; ?>features/slider-banner/Interactive-guide-01-img.png" alt="Interactive Guide for Hajj and Umrah Rituals">
                            <div class="carousel-caption">
                                <p>Interactive Guide for Hajj and Umrah Rituals</p>
                            </div>
                        </div>
                        <div class="item"> <img src="<?php echo IMG_URL; ?>features/slider-banner/Interactive-guide-02-img.png" alt="Interactive Guide for Hajj and Umrah Rituals">
                            <div class="carousel-caption">
                                <p>Interactive Guide for Hajj and Umrah Rituals</p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-arrow"> 
                        <a class="left carousel-control" href="#divRituals" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                        <a class="right carousel-control" href="#divRituals" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                    </div>
                </div>
              </td>
              </td>
              <td><p>You will get
                  All the necessary steps that are required to perform Hajj, based on day, time and location </p>
                <ul>
                  <li>All Duas or supplication that you are required to perform, based on your day, time and location</li>
                  <li>It will also show the path from one ritual to another ritual</li>
                </ul>
                </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
  </div>
</div>
<!--------------Listing pages End----------------> 
