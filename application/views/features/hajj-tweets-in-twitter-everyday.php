<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="row">
        <div class="nine columns">
            <h1>Hajj Tweets</h1>
            <div class="detail_description">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="custom-frame">
                                <div id="divHajjTweets" class="carousel slide div-featured-slide">
                                    <div class="carousel-inner">
                                        <div class="item active"> <img alt="Hajj Tweets" src="<?php echo IMG_URL; ?>features/slider-banner/hajj-tweets-01-img.png" alt="Hajj Tweets">
                                            <div class="carousel-caption">
                                                <p>Hajj Tweets</p>
                                            </div>
                                        </div>
                                        <div class="item"> <img alt="Hajj Tweets 2" src="<?php echo IMG_URL; ?>features/slider-banner/hajj-tweets-02-img.png" alt="Hajj Tweets">
                                            <div class="carousel-caption">
                                                <p>Hajj Tweets</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-arrow"> 
                                        <a class="left carousel-control" href="#divHajjTweets" data-slide="prev"> <i class="fa fa-angle-left"></i></a> 
                                        <a class="right carousel-control" href="#divHajjTweets" data-slide="next"> <i class="fa fa-angle-right"></i></a> 
                                    </div>
                                </div>
                            </td>
                            <td><ul><li>Connect with Social Network and get update on Hajj related Post or Post your own tweets.</li></ul></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="three columns"> <?php echo $this->load->view('layout/aside'); ?> </div>
    </div>
</div>
