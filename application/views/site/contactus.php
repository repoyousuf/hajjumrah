<section id="main">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <?php echo $this->load->view('layout/message'); ?>
            </div>
        </div>
        <div class="row">
            <div class="eight columns">
                <aside>
                    <div>
                        <section id="signUp" class="sign-up-form">
                            <h1>Contact Us</h1>
                            <div>
                                <form method="post" action="<?php echo site_url('site/contactus'); ?>" id="contactform" name="contactform">
                                    <div>
                                        <p>* Required fields</p>
                                    </div>
                                    <div> 
                                        <?php
                                        /* $result = '';
                                          $location = @file_get_contents('http://freegeoip.net/json/'.$_SERVER['REMOTE_ADDR']);

                                          $data     =    json_decode($location);
                                          if(!empty ($data))
                                          {
                                          $result  = get_country_dial_code(trim($data->country_code));
                                          } */
                                        $data = get_country_dial_code();
                                        ?>
                                        <label>Mobile<span class="required">*</span></label>
                                        <input name="dial_code" id="dial_code" value="" type="hidden" /> 
                                        <div class="custom-select"><i>Country Code </i>
                                            <select name="country_code" id="country_code">
                                                <option value="">--Select--</option>
                                                <?php foreach ($data AS $arr) { ?>
                                                    <option value="<?php echo ucwords($arr['dial_code']); ?>"><?php echo ucwords(strtolower($arr['country_name'])); ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="dial_code"></span>
                                            <br/><i>Your Number </i> <br/><input name="mobile" style="max-width: 384px;" id="mobile" value="" type="text" class="required number"/></div>
                                    </div>
                                    <div> 
                                        <label>Email <span class="required">*</span></label> 
                                        <input name="email" id="email" value="" type="text" class="required email"/>
                                    </div>
                                    <div> 
                                        <label>Query:<span class="required">*</span></label> 
                                        <textarea name="query" id="query" class="required"  onKeyDown="limitText(this.form.query,500);" onKeyUp="limitText(this.form.query, 500);"></textarea> 

                                        <div >(Maximum characters: 500). You have <span id="countdown" style="font-weight: bold; color: red; width: 30px;">500</span> characters left.</div>
                                    </div>
                                    <div> 
                                        <input value="Submit" class="button" type="submit" /> 
                                        <input value="Reset" class="button" type="reset" /> 
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </aside>                            
            </div>
            <div class="four columns">
                <aside>
                    <h2>Contact Information</h2>
                    <ul class="unstyled who">
                        <li><address><i class="fa fa-home"></i><strong>Address:</strong><br/> 
                                Dr. Mohamed Abdur Rahman, <br/>College of Comp. and Infor. Systems,<br/> Umm Al Qura University,<br/>
                                Abidia Campus, Makkah, KSA</address></li>
                        <li><strong>Saudi Arabia :</strong><br/>
                            <a href="mailto:hajjumrah@gistic.org"><i class="fa fa-envelope"></i>hajjumrah@gistic.org</a></li>
                        <li><strong>Bangladesh :</strong><br/>
                            <a href="mailto:advancedmedialab.bd@gmail.com"><i class="fa fa-envelope"></i>advancedmedialab.bd@gmail.com</a></li> 
                        <li><strong>India :</strong><br/>
                            <a href="mailto:hajisupport@designers-den.com"><i class="fa fa-envelope"></i>hajisupport@designers-den.com</a></li>
                        <li><strong>Web URL :</strong><br/><a href="<?php echo site_url(); ?>" rel="Web URL"><i class="fa fa-globe"></i>http://advancedmedialab.com/hajjumrah</a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</section>