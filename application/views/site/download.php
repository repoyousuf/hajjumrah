<?php
$iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
$iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
$webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");
?>

<section id="main" class="divDownloadpage">
  <div class="container">
    <div class="row">
      <div class="three columns">
        <div class="download-column">
          <div>
            <blockquote><i>This hajjumrah mobile app has been brought to you</i></blockquote>
            <p><span class="">by</span> <a target="_blank" href="<?php echo site_url(); ?>">Media Lab</a> </p>
          </div>
          <div class="download-apps" style="display: none;">
            <div class="hit-counter">
              <h4>Total Visits <strong><?php echo download_counter(); ?></strong></h4>
            </div>
          </div>
          <ul class="app-info common-box">
            <li><strong>Current version</strong><span>1.0</span> </li>
            <li><strong>Last Updated</strong><span>September 10, 2014</span> </li>
            <li><strong>File size</strong><span>7.5M</span> </li>
            <li><strong>Copyright</strong><span> <a href="http://www.advancedmedialab.com/">Advanced Media Laboratory</a></span> </li>
            <li><strong>Patent</strong><span> Pending</span> </li>
          </ul>
        </div>
        <aside>
          <section id="signUp" class="sign-up-form">
            <h1>Subscribe Here</h1>
            <div>
              <p class="intro"> To Get Instant Update Of Hajj & Umrah Services</p>
              <form method="post" action="<?php echo site_url('site/signup'); ?>" id="signupform" name="signupform">
                <div>
                  <p>* Required fields</p>
                </div>
                <div>
                  <label>Name<span class="required">*</span></label>
                  <input name="name" id="name" value="" type="text" class="required"/>
                </div>
                <div>
                  <?php
                                    $data = get_country_dial_code();
                                    ?>
                  <label>Mobile<span class="required">*</span></label>
                  <input name="dial_code" id="dial_code" value="" type="hidden" />
                  <div class="custom-select"><i>Country Code </i>
                    <select name="country_code" id="country_code">
                      <?php foreach ($data AS $arr) { ?>
                      <option value="<?php echo ucwords($arr['dial_code']); ?>"><?php echo ucwords(strtolower($arr['country_name'])); ?></option>
                      <?php } ?>
                    </select>
                    <span class="dial_code"></span> <br/>
                    <i>Your Number </i> <br/>
                    <input name="mobile" style="max-width: 384px;" id="mobile" value="" type="text" class="required number"/>
                  </div>
                </div>
                <div>
                  <label>Email <span class="required">*</span></label>
                  <input name="email" id="email" value="" type="text"  class="fn_email_exists required email"/>
                </div>
                <div>
                  <input value="Submit" class="button" type="submit" />
                  <input value="Reset" class="button" type="reset" />
                </div>
              </form>
            </div>
          </section>
        </aside>
      </div>
      <div class="nine columns">
        <div class="detail_description download-box">
          <table class="table">
            <tbody>
              <tr>
                <td class="custom-frame"><img src="<?php echo IMG_URL; ?>download-hajjumrah-mobile-app.png"/></td>
                <td>
               
                <h2>Our Hajjumrah <small>mobile app, are available now.</small> </h2>
                  <p> <small><i>Downloading this app is <strong>available</strong> for free for limited time from the following link:</i></small> <br/>
                    <br/>
                    <a href="https://play.google.com/store/apps/details?id=com.hajjandumrah&hl=en" title="Downloading Our Hajjumrah mobile application on google play store is available now" target="_blank"><img src="<?php echo IMG_URL; ?>Google-play-learge.png"/></a> <a href="https://itunes.apple.com/us/app/perform-hajj-umrah/id917265874?ls=1&mt=8" title="Downloading Our Hajjumrah mobile application on iTunes is available now" target="_blank"><img src="<?php echo IMG_URL; ?>download-on-the-app-store.png"/></a> </p>
                  <p><small><i>Get benefit from the application, which has a pack of features based on what a pilgrim needs at each context. It is very useful for pilgrims who are coming to Makkah this year or already in Makkah. Also, the family members of a pilgrim can be in touch with the pilgrim via the application. </i></small></p>
<hr/>
 <h2>Family Connect Download App </h2>
                
<a href="javascript:void(0);"> 
<img src="<?php echo IMG_URL; ?>family-lite-btn.png"/></a> 
<a href="javascript:void(0);"> 
<img src="<?php echo IMG_URL; ?>family-pro-btn.png"/></a>
<span style="display:block; font-size:20px; margin-top:20px;">Visit us later for this</span>
                  </td>
              </tr>
            </tbody>
          </table>
        </div>
        <hr/>
        <h1>Get benefit from our Hajjumrah Mobile Application<br/>
          <small>Using this, you will:</small></h1>
        <div id="divDownloadSlider" class="carousel slide">
          <div class="carousel-inner">
            <div class="item active"> <img src="<?php echo IMG_URL; ?>download-slider/01.jpg" alt="">
              <div class="carousel-caption">
                <p>Get, All the necessary steps that are required to perform Hajj & Umrah with Duas and Guideline</p>
              </div>
            </div>
            <div class="item"> <img src="<?php echo IMG_URL; ?>download-slider/02.jpg" alt="">
              <div class="carousel-caption">
                <p>Find the currency conversion rate and path/direction to your nearest money exchange store</p>
              </div>
            </div>
            <div class="item"> <img src="<?php echo IMG_URL; ?>download-slider/03.jpg" alt="">
              <div class="carousel-caption">
                <p>Have, Points of Interest like restaurants, hotels, Arafat toilets, Mina toilets etc.</p>
              </div>
            </div>
            <div class="item"> <img src="<?php echo IMG_URL; ?>download-slider/04.jpg" alt="">
              <div class="carousel-caption">
                <p>Know, The boundary of Haram, Mina, Muzdalifah and Arafat is a part of the Rituals of Hajj & Umrah.</p>
              </div>
            </div>
          </div>
          <div class="carousel-arrow"> <a class="left carousel-control" href="#divDownloadSlider" data-slide="prev"> <i class="fa fa-angle-left"></i> </a> <a class="right carousel-control" href="#divDownloadSlider" data-slide="next"> <i class="fa fa-angle-right"></i> </a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <?php echo $this->load->view('layout/welcomethis'); ?>
