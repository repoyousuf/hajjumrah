<section id="main">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <?php echo $this->load->view('layout/message'); ?>
            </div>
        </div>
        <div class="row">
            <div class="eight columns">
                <aside>
                    <div>
                        <section id="signUp" class="sign-up-form">
                            <h1>Subscribe Now</h1>
                            <div>
                                <form method="post" action="<?php echo site_url('site/signup'); ?>" id="signupform" name="signupform">
                                    <div>
                                        <p>* Required fields</p>
                                    </div>
                                     <div> 
                                        <label>Name <span class="required">*</span></label> 
                                        <input name="name" id="name" autocomplete="off" value="" type="text" class="required" onfocus=""/>
                                    </div> 
                                    <div> 
                                        <?php                                    
                                            $data = get_country_dial_code();
                                        ?>
                                        <label>Mobile<span class="required">*</span></label>
                                        <input name="dial_code" id="dial_code" value="" type="hidden" /> 
                                        <input name="subscribe" id="subscribe" value="1" type="hidden" /> 
                                        <div class="custom-select"><i>Country Code </i>
                                            <select name="country_code" id="country_code">
                                                 <option value="">--Select--</option>
                                                <?php foreach ($data AS $arr) { ?>
                                                    <option value="<?php echo ucwords($arr['dial_code']); ?>"><?php echo ucwords(strtolower($arr['country_name'])); ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="dial_code"></span>
                                            <br/><i>Your Number </i> <br/><input name="mobile" autocomplete="off" style="max-width: 384px;" id="mobile" value="" type="text" class="required number"/></div>
                                    </div>
                                    <div> 
                                        <label>Email <span class="required">*</span></label> 
                                        <input name="email" id="email" autocomplete="off" value="" type="text" class="fn_email_exists required email"/>
                                    </div>                                    
                                    <div> 
                                        <input value="Subscribe" class="button" type="submit" /> 
                                        <input value="Reset" class="button" type="reset" /> 
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </aside>                            
            </div>
            <div class="four columns">
                <aside>
                    <h2>Contact Information</h2>
                    <ul class="unstyled who">
                        <li><address><i class="fa fa-home"></i><strong>Address:</strong><br/> 
                                Dr. Mohamed Abdur Rahman, <br/>College of Comp. and Infor. Systems,<br/> Umm Al Qura University,<br/>
                                Abidia Campus, Makkah, KSA</address></li>
                        <li><strong>Saudi Arabia :</strong><br/>
                            <a href="mailto:hajjumrah@gistic.org"><i class="fa fa-envelope"></i>hajjumrah@gistic.org</a></li>
                        <li><strong>Bangladesh :</strong><br/>
                            <a href="mailto:advancedmedialab.bd@gmail.com"><i class="fa fa-envelope"></i>advancedmedialab.bd@gmail.com</a></li> 
                        <li><strong>India :</strong><br/>
                            <a href="mailto:hajisupport@designers-den.com"><i class="fa fa-envelope"></i>hajisupport@designers-den.com</a></li>
                        <li><strong>Web URL :</strong><br/><a href="<?php echo site_url(); ?>" rel="Web URL"><i class="fa fa-globe"></i>http://advancedmedialab.com/hajjumrah</a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</section>
