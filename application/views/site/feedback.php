<div class="container">
    <div class="row">
        <div class="three columns">
            <div class="left-pane-pad">
                <?php $this->load->view('layout/left_pane'); ?> 
            </div>
        </div>
        <div class="nine columns">    
            <script src="<?php echo JS_URL; ?>jquery.validate.js"></script>
            <script type="text/javascript">    
                $(document).ready(function(){        
                    $('#feedback_form').validate();
                });
            </script>
            <div class="row-fluid"> 
                <h1>
                    Feedback Tile
                </h1>
            </div>
            <div class="row-fluid">  
                <?php $this->load->view('layout/message'); ?> 
            </div>
            <div class="row-fluid"> 
                <div class="feedback"> 
                    <p class="note">All Fields are required.</p>
                    <?php echo form_open(site_url('site/feedback'), array('name' => 'feedback_form', 'id' => 'feedback_form'), ''); ?>
                    <div class="row-fluid">                
                        <input type="text" name="name" id="name" class="required" value="" placeholder="Write your name" />
                    </div> 
                    <div class="row-fluid">              
                        <input type="text" name="email" id="email" class="required email" value="" placeholder="Write your email" />
                    </div>
                    <div class="row-fluid">
                        <input type="text" name="url" id="url" class="required url" value="" placeholder="Write website url" />
                    </div> 
                    <div class="row-fluid">
                        <textarea  name="feedback" id="feedback" class="required" value="" placeholder="Write your query" style="height: 137px;"></textarea>
                    </div> 
                    <div class="row-fluid">                
                        <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Send"  />
                    </div> 
                    <?php echo form_close(); ?>
                </div>
            </div>   
        </div>
    </div>
</div>