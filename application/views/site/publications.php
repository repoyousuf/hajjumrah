<div class="container">
    <div class="row">
            <div class="twelve columns">
             <h1 title="Our Publications">Our Publications</h1>
            <div class="detail_description">
                <h3><mark>2014</mark></h3>
                <ul>
                    <p><strong>Track: Conference - Multimedia for e-Health</strong></p>
                    <li>
                        Ahmad Qamar, Imad Afyouni, Faizan Ur Rehman, Delwar Hossain, Asad Toonsi, Mohamed Abdur Rahman and Saleh Basalamah, "A Multimedia E-Health Framework Towards An Interactive And Non-Invasive Therapy Monitoring Environment", The 22nd ACM International Conference on Multimedia (ACM Multimedia), Orlando, Florida, USA, November 3-7, 2014 [CIS-UQU affiliation].
                    </li>
                    <p><strong>Track: Journal - Social Multimedia</strong></p>
                    <li>
                        Md. Abdur Rahman, Heung-Nam Kim, Abdulmotaleb El Saddik, and Wail Gueaieb. A context-aware multimedia framework toward personal social network services. Journal of MULTIMEDIA TOOLS AND APPLICATIONS. Springer US, vol. 71, no. 3, pp. 1717-1747, August 2014. Online link(UQU Affiliation).
                    </li>
                    <p><strong>Track: Journal - Multimedia for e-Learning</strong></p>
                    <li>
                        M. S. Hossain, A. Alghamdi, A. Alelaiwi, A. M. Ghoneim, and Md. Abdur Rahman, "QoS in Web Service-based Collaborative Engineering Education Environment", International Journal of Engineering Education Vol. 30, No. 3, pp. 618–624, 2014 [ISI-Indexed: 0.4].
                    </li>
                    <p><strong>Track: Journal - Social Media for Hajj and Umrah</strong></p>
                    <li>
                        Mohamed Abdur Rahman, Mohamed Ahmed, “Emotion-Based System for Social Media Content Processing and Event Monitoring”, Journal of Advances in Computing, Vol. 4, No. 2, 2014.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>