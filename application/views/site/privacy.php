<div class="container">
    <div class="row">
        <div class="three columns">
            <?php $this->load->view('layout/aside'); ?> 
        </div>
        <div class="nine columns">
            <h1 title="Hajjumrah Mobile App:: Privacy Policies">Privacy & Policy</h1>
            <div class="detail_description">
                <blockquote style="color: red;">we will not share your data with third party. If you register with us, we will simply notify you when we have any update in our software.</blockquote>
            </div>
        </div>
    </div>
</div>