<div class="container">
    <div class="row">
        <div class="three columns">
            <div class="left-pane-pad">
                <?php $this->load->view('layout/left_pane'); ?> 
            </div>
        </div>
        <div class="nine columns">
            <script src="<?php echo JS_URL; ?>jquery.validate.js"></script>
            <script type="text/javascript">    
                $(document).ready(function(){        
                    $('#feedback_form').validate();
                });
            </script>

            <div class="row-fluid"> 
                <h1>
                    Log In
                </h1>
            </div>
            <div class="row-fluid">  
                <?php $this->load->view('layout/message'); ?> 
            </div>
            <div class="row-fluid"> 
                <div>            
                    <?php echo form_open(site_url('site/login'), array('name' => 'feedback_form', 'id' => 'feedback_form'), ''); ?>
                    <div class="row-fluid">&nbsp;</div> 
                    <!--<div class="row-fluid">  
                        <div class="span3">
                            User Type
                        </div>
                        <div class="span8">                    
                            <select id="user_type" name="user_type" class="required" style="width: auto; height: auto;">
                                    <option value="">--Select--</option>
                                    <option value="1" >Employer</option>                                        
                                    <option value="2" >Employee</option> 
                             </select>
                        </div>
                    </div> -->
                    <div class="row-fluid">&nbsp;</div> 
                    <div class="row-fluid">  
                        <div class="span3">
                            Email
                        </div>
                        <div class="span8">
                            <input type="text" name="email" id="email" class="required email" value="" style="width: 220px; height: auto;" />
                        </div>
                    </div> 
                    <div class="row-fluid">&nbsp;</div> 
                    <div class="row-fluid">  
                        <div class="span3">
                            Password
                        </div>
                        <div class="span8">
                            <input type="password" name="password" id="password" class="required" value="" style="width: 220px; height: auto;"  />
                        </div>
                    </div>  
                    <div class="row-fluid">&nbsp;</div> 
                    <div class="row-fluid">  
                        <div class="span3">
                            &nbsp;
                        </div>
                        <div class="span8">
                            <p>Forgot your password? <a href="<?php echo base_url('site/forgot'); ?>">Click Here</a>.</p>                
                        </div>
                    </div>  

                    <div class="row-fluid">  
                        <div class="span3">
                            &nbsp;
                        </div>
                        <div class="span8">
                            <input type="submit" name="submit" id="submit" class="btn btn-primary" value="Log in"  />
                        </div>
                    </div>  
                    <?php echo form_close(); ?>
                </div>                
            </div>
            <?php $this->load->view('ads/add_728_90'); ?> 
        </div>
    </div>
</div>