<div class="container">
    <div class="row">
        <div class="three columns">
            <?php $this->load->view('layout/aside'); ?> 
        </div>
        <div class="nine columns">
            <h1>Video Tutorial of HajjUmrah Mobile App</h1>
            <hr/>
             <h3>Hajj Messenger</h3>
            <div id="videoYoutube" class="divVideoContainer">
                <iframe src="//www.youtube.com/embed/vVc7o9fZbFg" frameborder="0" allowfullscreen></iframe>
            </div>
<hr class="hidden"/>
<h3>Adding a new friend or pilgrim or family member</h3>
            <div id="videoYoutube-01" class="divVideoContainer">
                <iframe src="//www.youtube.com/embed/dNbdJ9hmBUs" frameborder="0" allowfullscreen></iframe>
            </div>

<hr class="hidden"/>
<h3>Features</h3>
            <div id="videoYoutube-01" class="divVideoContainer">
               <iframe src="//www.youtube.com/embed/9R5lZcWSkqY" frameborder="0" allowfullscreen></iframe>
            </div>

<hr class="hidden"/>
<h3>Registering Hajj and Umrah Application</h3>
            <div id="videoYoutube-01" class="divVideoContainer">
               <iframe src="//www.youtube.com/embed/RKlpQqVTilM" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>