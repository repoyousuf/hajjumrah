<div class="container">
    <div class="row">
        <div class="three columns">
            <div class="div_tabs">
                <ul>
                    <li class="selected"><a class="divVideos" href="javascript:void(0);">Videos</a> </li>
                    <li> <a class="divManuals" href="javascript:void(0);">Manuals</a> </li>
                    <li> <a class="divFaq" href="javascript:void(0);">Faq</a> </li>
                </ul>
            </div>
            <section class="utility-follow-sec">
                <h2>Follow Us</h2>
                <ul class="link-list social">
                    <li class="facebook"><a target="_blank" href="https://www.facebook.com/pages/Hajj-Umrah-Mobile-App/1486164448292981">Facebook</a></li>
                    <li class="googleplus"><a target="_blank" href="https://plus.google.com/u/2/b/110508884895775157167/110508884895775157167/about">Google+</a></li>
                    <li class="twitter"><a target="_blank" href="https://twitter.com/advMediaLabBD">Twitter</a></li>
                    <li class="youtube"><a target="_blank" href="https://www.youtube.com/user/theadvancedmedialab">YouTube</a></li>
                    <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/pub/advanced-media-lab/a3/570/21a">Linkedin</a></li>
                </ul>
            </section>
        </div>
        <div class="nine columns"> 
            <!-- div Video Details Data-->
            <div id="divVideos" class="dvDataContainer" style="display: block;">
                <div class="row">
                    <div class="columns nine">
                        <h1>Video Tutorial of HajjUmrah Mobile App</h1>
                    </div>
                    <div class="columns three">
                        <div>Select language :</div>
                        <div class="customSelect">
                           <select id="fn_language" name="language">
                                <option value="english">English</option>
                                <option value="bangla">Bangla</option>
                                <option value="arabic">Arabic</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <!-- Videos in English Language -->
                    <div id="divVideosInEnglish" class="fn_video_block english"  style="display: block;">
                        <h3>Registering Hajj and Umrah Application</h3>
                        <div id="videoYoutubeRegisterEN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/RKlpQqVTilM" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Features</h3>
                        <div id="videoYoutubeFeaturesEN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/9R5lZcWSkqY" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Adding a new friend or pilgrim or family member</h3>
                        <div id="videoYoutubeAddNewFriendEN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/dNbdJ9hmBUs" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Hajj Messenger</h3>
                        <div id="videoYoutubehajjMessengerEN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/EiGdfak67vo" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                    <!-- Videos in Bengali Language -->
                    <div id="divVideosInBengali" class="fn_video_block bangla" style="display: none;">
                        <h3>Registering Hajj and Umrah Application</h3>
                        <div id="videoYoutubeRegisterBN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/p8HAvNtS3TA" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Features Bangla</h3>
                        <div id="videoYoutubeFeaturesBN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/ed8DBD_2Y_w" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Adding a new friend or pilgrim or family member</h3>
                        <div id="videoYoutubeAddNewFriendBN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/UrKzQgcWJX8" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <hr class="hidden"/>
                        <h3>Hajj Messenger</h3>
                        <div id="videoYoutubehajjMessengerBN" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/TGlWWJ-fUKQ" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>                    
                       <!-- Videos in arabic Language -->
                    <div id="divVideosInArabic" class="fn_video_block arabic"  style="display: none;">
                        <h3>How to add a friend</h3>
                        <div id="videoYoutubeRegisterARB" class="divVideoContainer">
                            <iframe src="https://www.youtube.com/embed/-7l391ULC7M" frameborder="0" allowfullscreen></iframe>
                        </div>                      
                    </div>
                </div>
            </div>
            <!-- div Manulas Details Data-->
            <div id="divManuals" class="dvDataContainer">
                <h1>Welcome to the Hajjumrah Mobile app Manuals</h1>
                <p>Hajj and Umrah applicationhas been designed to provide pilgrims all the necessary steps required performing Hajjbased on user’s day, time and location. The app will also help you to track Fard, Wajib and Sunnah of Hajj, based on your current location. More so, it will help you to make your hajj perfect. </p>
                <div class="divDownloadThis">
                    <div class="manuals-app-pdf">
                        <p>Click here to download  PDF Manual</p>
                        <a target="_blank" href="<?php echo base_url('assets/images/pdf/User-Manual-Hajj&UmrahApp.pdf'); ?>"><img alt="Emergency Services" src="<?php echo IMG_URL; ?>pdf-donwload.png"/></a>
                    </div>
                    <div class="manuals-app-doc">
                        <p>Click here to download docx Manual</p>
                        <a href="<?php echo base_url('assets/images/doc/UserManual-for-Hajj&Umrah-Mobile-App.docx'); ?>"><img alt="Emergency Services" src="<?php echo IMG_URL; ?>doc-donwload.png"/></a>
                    </div>
                </div>
            </div>
            <!-- div FAQ Details Data-->
            <div id="divFaq" class="dvDataContainer">
                <h1>Frequently Ask Question in Media Lab</h1>
                <div class="faq_container">
                    <div class="faq_header">
                        <h3><a href="javascript:void(0);">Common Questions</a></h3>
                    </div>
                    <div class="faq_item_list">
                        <div class="faq_panel"> <a href="javascript:void(0);" class="faq_question collapsed"> <span><i class="fa fa-caret-right"></i> </span> <span>How do I Download hajjumrah mobile application?</span> </a>
                            <div class="faq_answer expanded">
                                <div>
                                    <p>Hajj and Umrah applicationhas been designed to provide pilgrims all the necessary steps required performing Hajjbased on user’s day, time and location. The app will also help you to track Fard, Wajib and Sunnah of Hajj, based on your current location. More so, it will help you to make your hajj perfect. </p>
                                    <div class="manuals-app">
                                        <p><strong>Click the icon</strong> below to download Hajjumrah mobile apk pdf manual </p>
                                        <a target="_blank" href="<?php echo base_url('assets/images/pdf/User-Manual-Hajj&UmrahApp.pdf'); ?>"><img alt="Emergency Services" src="<?php echo IMG_URL; ?>pdf-donwload.png"/></a>
                                    </div>               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

