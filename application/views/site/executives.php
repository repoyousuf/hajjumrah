<div class="container">
    <div class="row">
        <div class="twelve columns">
            <h1>Members <small>we work together</small></h1>
            <hr class="hidden"/>
            <div class="detail_description">
                <h3>Founder and Director</h3>
                <hr class="hidden"/>
                <ul class="members-profile-list">
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                            <div><strong>Dr. Mohamed Abdur Rahman (Latest CV)</strong></div>
                            <label>(Founder & Director)</label>
                            <label>CS Department</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                        <div>
                            <strong>Dr. Saleh Basalamah</strong></div>
                            <label>(Founder)</label>
                            <label>Deputy Director, GIS , <br/>TIC</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                </ul>
                <hr/>
                <h3>Co-Investigators</h3>
                <hr class="hidden"/>
                <ul class="members-profile-list">
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                            <div><strong>Dr. Mohamed Abdur Rahman (Latest CV)</strong></div>
                            <label>(Founder & Director)</label>
                            <label>CS Department</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                        <div>
                            <strong>Dr. Saleh Basalamah</strong></div>
                            <label>(Founder)</label>
                            <label>Deputy Director, GIS , <br/>TIC</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                </ul>
                <hr/>
                <h3>International Collaborators</h3>
                <hr class="hidden"/>
                <ul class="members-profile-list">
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                            <div><strong>Dr. Mohamed Abdur Rahman (Latest CV)</strong></div>
                            <label>(Founder & Director)</label>
                            <label>CS Department</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                        <div>
                            <strong>Dr. Saleh Basalamah</strong></div>
                            <label>(Founder)</label>
                            <label>Deputy Director, GIS , <br/>TIC</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                </ul>
                <hr/>
                <h3>Researchers</h3>
                <hr class="hidden"/>
                <ul class="members-profile-list">
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                            <div><strong>Dr. Mohamed Abdur Rahman (Latest CV)</strong></div>
                            <label>(Founder & Director)</label>
                            <label>CS Department</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                        <div>
                            <strong>Dr. Saleh Basalamah</strong></div>
                            <label>(Founder)</label>
                            <label>Deputy Director, GIS , <br/>TIC</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                </ul>
                <hr/>
                <h3>Researcher & Administrator</h3>
                <hr class="hidden"/>
                <ul class="members-profile-list">
                    <li>
                        <a class="profile-img" href="javascript:void(0);"><img src="<?php echo base_url(''); ?>assets/images/thumb-pic.png" alt="Members Profile"/></a>
                        <p>
                            <div><strong>Dr. Mohamed Abdur Rahman (Latest CV)</strong></div>
                            <label>(Founder & Director)</label>
                            <label>CS Department</label>
                            <label>Umm Al Qura University</label>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>