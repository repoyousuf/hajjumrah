<?php
$iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
$iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
$iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
$webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");
?>
<div class="container">
    <div class="row">
        <div class="three columns">
            <?php $this->load->view('layout/aside'); ?> 
        </div>
        <div class="nine columns">
            <h1>Welcome to the Hajjumrah Mobile app Manuals</h1>
            <article class="div-manuals-list" role="manuals">
                <h2 class="accordian-title">Step 1: Download the app</h2>
                <ul class="accordion-ul">
                    <li class="selected">
                        <a href=""><h3>Download Our Hajjumrah App</h3></a>
                        <div style="display: block;">
                            <ul>
                                <li>
                                    <a href="">Download the app</a>
                                </li>
                                <li>
                                    <a href="">Install the app to your mobile</a>
                                </li>
                                <li>
                                    <a href="">Creat an account</a>
                                </li>
                                <li>
                                    <a href="">Fill the data</a>
                                </li>
                                <li>
                                    <a href="">Personalize your features</a>
                                </li>
                                <li>
                                    <a href="">Enjoy the benefit</a>
                                </li>
                            </ul>
                        </div>
                        <div class="img-container" >
                            <div>
                                <img alt="Download Our Hajjumrah App" src="<?php echo IMG_URL; ?>download-app-notebok.png"/>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href=""><h3>Install the app to your mobile</h3></a>
                        <div>
                            <ul>
                                <li>
                                    <a href="">Download the app</a>
                                </li>
                                <li>
                                    <a href="">Install the app to your mobile</a>
                                </li>
                                <li>
                                    <a href="">Create an account</a>
                                </li>
                                <li>
                                    <a href="">Fill the data</a>
                                </li>
                                <li>
                                    <a href="">Personalize your features</a>
                                </li>
                                <li>
                                    <a href="">Enjoy the benefit</a>
                                </li>
                            </ul>
                        </div>
                        <div class="img-container">
                            <div>
                                <img alt="Download Our Hajjumrah App" src="<?php echo IMG_URL; ?>download-app-notebok.png"/>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a href=""><h3>Create an account</h3></a>
                        <div>
                            <ul>
                                <li>
                                    <a href="">Download the app</a>
                                </li>
                                <li>
                                    <a href="">Install the app to your mobile</a>
                                </li>
                                <li>
                                    <a href="">Create an account</a>
                                </li>
                                <li>
                                    <a href="">Fill the data</a>
                                </li>
                                <li>
                                    <a href="">Personalize your features</a>
                                </li>
                                <li>
                                    <a href="">Enjoy the benefit</a>
                                </li>
                            </ul>
                        </div>
                        <div class="img-container">
                            <div>
                                <img alt="Download Our Hajjumrah App" src="<?php echo IMG_URL; ?>download-app-notebok.png"/>
                            </div>
                        </div>
                    </li>
                </ul>
            </article>
        </div>
    </div>


    <div class="row">
        <div class="twelve columns">
            <div class="common-box clearfix">
                <div class="four columns">
                    <div class="program-title">
                        <h2>Hajjumrah <span class="item-version">Current version 1</span></h2>
                        <div class="powerby"> <span class="">by</span> <a target="_blank" href="http://www.viber.com/">Media Lab</a> </div>
                    </div>
                </div>
                <div class="four columns"> 
                    <div class="download-apps">
                        <?php if ($iPod || $iPhone || $iPad) { ?>
                            <div class="button green-button">
                                <a href="http://viber.downloadable.co/download/?pid=50298&amp;vid=205770&amp;page2=1"> <strong>Download <span class="download-arrow"></span></strong> <small class="available">Available App for Hajjumrah</small> <i>Get this app for <span>IOS</span></i> </a> 
                            </div>
                        <?php } else if ($Android) { ?>
                            <div class="button green-button">
                                <a href="http://viber.downloadable.co/download/?pid=50298&amp;vid=205770&amp;page2=1"> <strong>Download <span class="download-arrow"></span></strong> <small class="available">Available App for Hajjumrah</small> <i>Get this app for <span>Android</span></i></a> 
                            </div>
                        <?php } else { ?>
                            <div class="button green-button">
                                <a href="http://viber.downloadable.co/download/?pid=50298&amp;vid=205770&amp;page2=1"> <strong>Download <span class="download-arrow"></span></strong> <small class="available">Available App for Hajjumrah</small></a> 
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>