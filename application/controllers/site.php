<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Site extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('site_model', 'site');
    }

    public function index() {
        $data = array();
        $data['activeTab'] = 'home';
        $this->layout->title('Hajj & Umrah mobile app, A complete guidance for pilgrims and their family members');
        $this->layout->view('index', $data);
    }

    public function features() {
        $featured_slug = $this->uri->segment(3);
        $data = array();
        $data['activeTab'] = 'features';
        $title = ucwords(str_replace('-', ' ', $featured_slug));
        $this->layout->title($title);
        $this->layout->view('features/' . $featured_slug, $data);
    }

    public function whatweoffer() {
        $data = array();
        $data['activeTab'] = 'whatweoffer';
        $this->layout->title('Features of Hajj & Umrah Mobile app');
        $this->layout->view('site/whatweoffer', $data);
    }

    public function projects() {
        $data = array();
        $data['activeTab'] = 'projects';
        $this->layout->title('Funded Projects');
        $this->layout->view('site/projects', $data);
    }

    public function publications() {
        $data = array();
        $data['activeTab'] = 'publications';
        $this->layout->title('Publications');
        $this->layout->view('site/publications', $data);
    }

    public function executives() {
        $data = array();
        $data['activeTab'] = 'executives';
        $this->layout->title('Executives Body');
        $this->layout->view('site/executives', $data);
    }

    public function tutorials() {
        $data = array();
        $data['activeTab'] = 'tutorials';
        $this->layout->title('FAQ Video Tutorials');
        $this->layout->view('site/tutorials', $data);
    }

    public function videos() {
        $data = array();
        $data['activeTab'] = 'tutorials';
        $this->layout->title('Video Tutorials of Hajj Umrah');
        $this->layout->view('site/videos', $data);
    }

    public function manuals() {
        $data = array();
        $data['activeTab'] = 'tutorials';
        $this->layout->title('Manuals Of HajjUmrah Mobile Applications');
        $this->layout->view('site/manuals', $data);
    }

    public function faq() {
        $data = array();
        $data['activeTab'] = 'tutorials';
        $this->layout->title('FAQs Of HajjUmrah');
        $this->layout->view('site/faq', $data);
    }

    public function subscribe() {
        $data = array();
        $data['activeTab'] = 'subscribe';
        $this->layout->title('Subscribe for Hajj & Umrah Update');
        $this->layout->view('site/subscribe', $data);
    }
    
    public function download() {
        $data = array();
        $data['activeTab'] = 'download';
        $this->layout->title('Download Our Hajj & Umrah Apps, now its free for all android users');
        $this->layout->view('site/download', $data);
    }

    public function contactus() {
        if ($_POST) {
            $masg = '';
            if ($this->sendContactMail()) {
                $this->session->set_flashdata('success', 'Your contact information sent successfully.');                
                $masg = 'success';
            } else {
                $this->session->set_userdata('error',  'Your contact information send failed. Please try again later.');
                 $masg = 'error';
            }
            
            if($_POST['main_site'] == 1)
            {
                
                //header('Location:http://advancedmedialab.com/contact.php?msg'.$masg);
                 redirect('http://advancedmedialab.com/contact.php?msg='.$masg);
            }
             redirect('site/contactus');
        }

        $data = array();
        $data['activeTab'] = 'contactus';
        $this->layout->title('Contact Us');
        $this->layout->view('site/contactus', $data);
    }

    public function signup() {
        if ($_POST) {
            $data = array();
            $data['name'] = $this->input->post('name');
            $data['mobile'] = $this->input->post('dial_code') . ' ' . $this->input->post('mobile');
            $data['email'] = $this->input->post('email');
            if ($this->site->insert('ho_subscribers', $data)) {
                $this->sendSignupMailToAdmin();
                $this->sendSignupMailToMember();
                $this->session->set_flashdata('success', 'You have successfully signed up.');
            } else {
                $this->session->set_userdata('error', 'Your sign up trying has been failed. Please try anothet time.');
            }
            
            if($this->input->post('subscribe'))
            {
                 redirect('site/subscribe');
            }else{
                 redirect('site');
            }
           
        }
    }

    public function duplicate_email_check() {
        if ($this->input->post('email')) {
            if ($this->site->get_single('ho_subscribers', array('email' => $this->input->post('email')))) {
                echo TRUE;
            } else {
                echo FALSE;
            }
        }
    }

    private function sendContactMail() {
    
       // $to   = 'yousuf361@gmail.com';
       // $to   = 'ihzaka100@gmail.com';
         $to = 'advancedmedialab.bd@gmail.com';
        $from = $this->input->post('email');

        $subject = 'Contact information from Medialad-Hajjumrah.';        
       
        
        $code =  $this->input->post('dial_code') ? $this->input->post('dial_code') : '';

      
          $body = "<table style='font-family: Arial; font-size: 14px; color: #000; border: 1px solid #e0e5e9; width: 100%; max-width:560px; padding: 15px 25px; background-color: #f2f2f3; margin-bottom: 1em;'>
                    <tbody>
                        <tr>
                            <td style='font-size: 14px; color: #000; padding:10px;'>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Mobile:</strong> " .$code . ' ' . $this->input->post('mobile') . "</p>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Email:</strong> " . $this->input->post('email') . "</p>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Query:</strong> " . $this->input->post('query') . "</p>
                                <p style='padding: 5px 0; height:10px; margin:0 0 8px;'></p>
                                <p style='clear: both; display:table; content:'';'></p>
                            </td>
                        </tr>
                    </tbody>
                </table>";

         $this->load->library('email');
        $this->email->clear();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($from, 'HajjUmrah Media Lab');
        $this->email->to($to);
        $this->email->bcc('ihzaka100@gmail.com');
        $this->email->subject($subject);
        $this->email->message($body);
        // $this->email->send();
       
       if($this->email->send()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    private function sendSignupMailToAdmin() {
        //$to   = 'yousuf361@gmail.com';
        //$to   = 'syedabdullah183@gmail.com';
         $to = 'advancedmedialab.bd@gmail.com';
        $from = $this->input->post('email');
        $subject = 'Subscription from Medialad-Hajjumrah.';
        
         $code =  $this->input->post('dial_code') ? $this->input->post('dial_code') : '';

        $body = "<table style='font-family: Arial; font-size: 14px; color: #000; border: 1px solid #e0e5e9; width: 100%; max-width:560px; padding: 15px 25px; background-color: #f2f2f3; margin-bottom: 1em;'>
                    <tbody>
                        <tr>
                            <td style='font-size: 14px; color: #000; padding:10px;'>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Name:</strong> " . $this->input->post('name') . "</p>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Mobile:</strong> " . $code . ' ' . $this->input->post('mobile') . "</p>
                                <p style='padding: 5px 0; margin:0 0 8px;'><strong>Email:</strong> " . $this->input->post('email') . "</p>
                                <p style='padding: 5px 0; height:10px; margin:0 0 8px;'></p>                              
                                <p style='padding: 5px 0; margin:0 0 8px;'>" . $this->input->post('name') . "</p>
                                <p style='clear: both; display:table; content:'';'></p>
                            </td>
                        </tr>
                    </tbody>
                </table>";

        $this->load->library('email');
        $this->email->clear();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($from, 'HajjUmrah Media Lab');
        $this->email->to($to);
        $this->email->bcc('ihzaka100@gmail.com');
        $this->email->subject($subject);
        $this->email->message($body);
        // $this->email->send();
       
       if($this->email->send()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    private function sendSignupMailToMember() {
        $to = $this->input->post('email');
        //$from   = 'yousuf361@gmail.com';
        //$from   = 'syedabdullah183@gmail.com';
        $from = 'advancedmedialab.bd@gmail.com';
        $subject = 'Subscribe confirmation in Medialad-Hajjumrah.';

        $body = "<table style='font-family: Arial; font-size: 14px; color: #000; border: 1px solid #e0e5e9; width: 100%; max-width:560px; padding: 15px 25px; background-color: #f2f2f3; margin-bottom: 1em;'>
                      <tbody>
                        <tr>
                            <td style='font-size: 14px; color: #000; padding:10px;'>
                            <p style='padding: 5px 0; margin:0 0 8px;'> Hi!  " . $this->input->post('name') . "</p>
                            <p style='padding: 5px 0; margin:0 0 8px;'>Thank you very much for subscribe in Medialab Hajj Umrah. </p>
                            <p style='padding: 5px 0; margin:0 0 8px;'>We are here for your all kinds of support</p>
                            <p style='padding: 5px 0; margin:0 0 8px;'></p>
                            <p style='padding: 5px 0; margin:0 0 8px;'>Thank you for being connected with us.</p>
                            <h4 style='padding: 5px 0; margin:0 0 8px;'>Medialab Team</h4>
                            </td>
                        </tr>
                    </tbody>
                </table>";

        $this->load->library('email');
        $this->email->clear();
        $config['mailtype'] = "html";
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");

        $this->email->from($from, 'HajjUmrah Media Lab');
        $this->email->to($to);
        $this->email->bcc('ihzaka100@gmail.com');
        $this->email->subject($subject);
        $this->email->message($body);
         // $this->email->send();
       
       if($this->email->send()){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function webadmin() {
        $data = array();
        $this->layout->title('Hajj Umrah - Web Admin');
        $this->layout->view('site/webadmin', $data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */