<?php

if(!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    // insert new data
    function insert($table_name, $data_array)
    {
        $this->db->insert($table_name, $data_array);
        return $this->db->insert_id();
    }

    // insert new data
    function insert_batch($table_name, $data_array)
    {
        $this->db->insert_batch($table_name, $data_array);
        return $this->db->insert_id();
    }

    // insert new data with autoincrement id
    function insert_ignore_auto($table_name, $data_array)
    {
        $sql = $this->db->insert_string($table_name, $data_array);
        $sql = str_replace('INSERT', 'INSERT IGNORE', $sql);
        $success = $this->db->query($sql);
        if($success):
            return $this->db->insert_id();
        else:
            return FALSE;
        endif;
    }

    // insert new data without autoincrement field
    function insert_ignore($table_name, $data_array)
    {
        $sql = $this->db->insert_string($table_name, $data_array);
        $sql = str_replace('INSERT', 'INSERT IGNORE', $sql);
        $success = $this->db->query($sql);
        if($success):
            return $this->db->affected_rows();
        else:
            return FALSE;
        endif;
    }

    // update data by index
    function update($table_name, $data_array, $index_array)
    {
        $this->db->update($table_name, $data_array, $index_array);
        return $this->db->affected_rows();
    }

    // delete data by index
    function delete($table_name, $index_array)
    {
        $this->db->delete($table_name, $index_array);
        return $this->db->affected_rows();
    }


    public function get_list($table_name, $index_array, $columns = null, $limit = null , $offset = 0, $order_type = null )
    {
        if($columns)
            $this->db->select($columns);
        if($limit)
            $this->db->limit($limit, $offset);

        if($order_type)
        $this->db->order_by('created', $order_type);
        else
        $this->db->order_by('modified', 'desc');
        
        $this->db->order_by('id', 'DESC');

        return $this->db->get_where($table_name, $index_array)->result();
    }

    // get data list by index order
    function get_list_order($table_name, $index_array, $order_array, $limit = null)
    {
        if($limit)
        {
            $this->db->limit($limit);
        }

        if($order_array)
        {
            $this->db->order_by($order_array['by'], $order_array['type']);
        }
        else
        {
            $this->db->order_by('created', 'desc');
        }

        return $this->db->get_where($table_name, $index_array)->result();
    }

    // get single data by index
    function get_single($table_name, $index_array, $columns = null)
    {
        if($columns)
            $this->db->select($columns);
        $this->db->limit(1);
        $row = $this->db->get_where($table_name, $index_array)->row();
        return $row;
    }

    // get number of rows in database
    function count_all($table_name)
    {
        return $this->db->count_all($table_name);
    }

    // get data with paging
    function get_paged_list($table_name, $index_array, $url, $segment, $offset = 0, $order_by = null )
    {
        $result = array('rows' => array(), 'total_rows' => 0);

        $this->load->library('pagination');

        $limit = $this->config->item('admin_per_page');

        $this->db->where($index_array);
        $this->db->order_by('status', 'desc');
        $this->db->order_by('modified', 'desc');
        
        if($order_by){
            $this->db->order_by('is_read', 'DESC');
            $this->db->order_by('created', 'DESC');
        }
        
        $result['rows'] = $this->db->get($table_name, $limit, $offset)->result();

        $this->db->where($index_array);
        $result['total_rows'] = $total_rows = $this->db->count_all_results($table_name);
        


        $config['uri_segment'] = $segment;
        $config['base_url'] = site_url() . $url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $this->config->item('admin_per_page');

        $this->pagination->initialize($config);
        $result['pagination'] = $this->pagination->create_links();

        return $result;
    }

// get data with paging
    function get_paged_list_order($table_name, $index_array, $order_array, $limit = 10, $offset = 0)
    {
        $result = array('rows' => array(), 'total_rows' => 0);

        if($order_array)
        {
            $this->db->order_by($order_array['by'], $order_array['type']);
        }
        else
        {
            $this->db->order_by('created', 'desc');
        }

        $this->db->where($index_array);
        $result['rows'] = $this->db->get($table_name, $limit, $offset)->result();

        $this->db->where($index_array);
        $result['total_rows'] = $this->db->count_all_results($table_name);

        return $result;
    }

    public function send_email($mail_info)
    {

        $this->load->library('email');

        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $from = $mail_info['from'] ? $mail_info['from'] : '';
        $from_name = $mail_info['from_name'] ? $mail_info['from_name'] : '';
        $to = $mail_info['to'] ? $mail_info['to'] : 'yousuf361@gmail.com';
        $cc = $mail_info['cc'] ? $mail_info['cc'] : '';
        $bcc = $mail_info['bcc'] ? $mail_info['bcc'] : '';
        $subject = $mail_info['subject'] ? $mail_info['subject'] : '';
        $message = $mail_info['message'] ? $mail_info['message'] : '';

        $this->email->from($from, $from_name);
        $this->email->to($to);
        $this->email->cc($cc);
        $this->email->bcc($bcc);
        $this->email->subject($subject);
        $this->email->message($message);

        return ($this->email->send()) ? TRUE : FALSE;

        //echo $this->email->print
    }

    public function delete_file($file_path)
    {
        if(file_exists($file_path))
        {
            @unlink($file_path);
        }
    }

    // get single data by index
    function get_single_order($table_name, $index_array, $order_array, $columns = null)
    {
        if($columns)
            $this->db->select($columns);
        $this->db->limit(1);
        if($order_array)
        {
            $this->db->order_by($order_array['by'], $order_array['type']);
        }
        else
        {
            $this->db->order_by('created', 'desc');
        }
        $row = $this->db->get_where($table_name, $index_array)->row();
        return $row;
    }

}

?>